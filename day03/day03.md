

# Day03

## 1:交换机

### 1-1: 中继器

​      网络， 局域网（Lan）的出现其最先解决了网共享和整合计算机的运算能力：

​      首先我们来看一下他们是如何工作的：

​      PC1/PC2相互通信，就必须有一个介质将他们连接起来（跳线），并配置好Ip地址。但是，网线上传输信号的时候是通过电信号表示的，所以网线上传输信号

的距离是有限制的（100M）。如果PC1/PC2的距离特别远，为了能够让信号正常传输，我们必须在PC1/PC2中间加一个设备来放大这个电信号，我们把这个叫做-

--中继器。

​      但是，那就是中继器的缺点在于：他仅仅只有两个口，所以，只能连接少量的设备。

### 1-2 : 集线器

​      如果想要连接更多的设备，只能使用-- 集线器，集线器是一个多个中继器组成的设备（多端口的中继器）。

​      暂时解决了节点多的问题，这时有伴随出现了另一个新问题，那就是“信号冲突”，

<img src="../day03/images/hub.jpg" style="zoom: 67%;" />

为了解决上述的“信号冲突”问题。我们可以

​       ---csma/cd 带有冲突检测的载波侦听多路访问“机制”

​      ---分割冲突域 即分割“电信号冲突的范围” 

​      ---网桥 （bridge）网桥处理的是数据帧

​    中继器 ：仅仅只有2个端口   节点少  没有冲突 

​    集线器： 端口多，但是只有一个冲突域  数据老是冲突，传输失败

​    交换机：我们在HUB里建 产一个网桥机制，把以前的电信号处理来数据帧进行数据交换（由原来的

第一层变成第二层，所以我们有把他们称为二层交换机）（mac地址表老化时间为300s,300S更新一次）

### 1-3: 交换机

交换机的出现就解决了冲突域问题，因为交换机的每一个接口都是一个冲突域。他工作在OSI模型的第二层，他处理是数据帧。

![](../day03/images/switch.jpg)

#### 1-3-1：MAC地址 

查看交换机的MAC地址和交换机MAC地址表

```JAVA
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.	
<Huawei>display mac-address                 //目前交换机没有数据进行交换，所以MAC地址表不空
    	
<Huawei>display bridge mac-address          //查看交换机的MAC地址
System bridge MAC address: 4c1f-cc10-6d42   //交 换机的MAC地址为：4c1f-cc10-6d42   16进制
<Huawei>
```

PC1/PC2相互通信

```
PC>ipconfig

Link local IPv6 address...........: fe80::5689:98ff:fe1c:49fa
IPv6 address......................: :: / 128
IPv6 gateway......................: ::
IPv4 address......................: 172.199.0.1
Subnet mask.......................: 255.255.255.192
Gateway...........................: 0.0.0.0
Physical address..................: 54-89-98-1C-49-FA
DNS server........................:

PC>ping 172.199.0.2

Ping 172.199.0.2: 32 data bytes, Press Ctrl_C to break
From 172.199.0.2: bytes=32 seq=1 ttl=128 time=31 ms
From 172.199.0.2: bytes=32 seq=2 ttl=128 time=31 ms
From 172.199.0.2: bytes=32 seq=3 ttl=128 time=47 ms
From 172.199.0.2: bytes=32 seq=4 ttl=128 time=31 ms
From 172.199.0.2: bytes=32 seq=5 ttl=128 time=47 ms

--- 172.199.0.2 ping statistics ---
  5 packet(s) transmitted
  5 packet(s) received
  0.00% packet loss
  round-trip min/avg/max = 31/37/47 ms

PC>
```

查看路由器的MAC地址表

```
<Huawei>display mac-address
MAC address table of slot 0:
-------------------------------------------------------------------------------
MAC Address    VLAN/       PEVLAN CEVLAN Port            Type      LSP/LSR-ID  
               VSI/SI                                              MAC-Tunnel  
-------------------------------------------------------------------------------
5489-981c-49fa 1           -      -      GE0/0/1         dynamic   0/-         
-------------------------------------------------------------------------------
Total matching items on slot 0 displayed = 1 

<Huawei>
```

目录的一条MAC地址

MAC地址表更新时间

```java
<Huawei>display mac-address aging-time    //老化时间为300S

  Aging time: 300 seconds
<Huawei>
```

总结：交换机是使用MAC地址表进行数据转发的。

交换机速率类型：

​         全双工   同一个时间段，既能发，有能收

​         半双工   同一时间段，只么发，只些收，只能做一样

1-3-2：交换机工作原理

​            学习：通过学习数据帧的源MAC地址来形成MAC地址表

​            广播：若目标地址在MAC地址表中没有，交换机就向该数据帧的来源端口外的其他所有的端口广播该数据帧

​           转发：交换机根据MAC地址表和目标MAC地址，单播转发数据帧

​           更新：交换机MAC地址表中的转发条目的老化时间为300秒，如果发现一个帧的入端口和MAC地址表中的源MAC地址所对应的端口不同，交换机将MAC地址重新学习到新的端口。

​           另外：

​                 1：交换机的MAC地址表中的条目类型为：

​                        --静态MAC地址：设备重启后，不会丢失

​                        --动态MAC地址：设备重启后，MAC地址条目丢失；

​                2：交换机的MAC地址表，是有一定大小的。

```java
<Huawei>display mac-address summary   //查看交换机mac地址表总条目数
Summary information of slot 0:
-----------------------------------
Static     :               0
Blackhole  :               0
Dynamic    :               0
Sticky     :               0
Security   :               0
Authen     :               0
Guest      :               0
Mux        :               0
In-used    :               0
Capacity   :               32768    //总条目数
-----------------------------------
```

#### 1-3-2：广播域：

​             数据广播的范围  ，即一个广播域就是一个网段

​            经过我们对交换机工作原理的分析，我们发现，交换机虽然可以隔离冲突域，但是无法隔离广播域，并且，实际的工作环境中的大部分流量恶意流量 ，都是以广播形式发出的。所以，交换机会将恶意流量发送到全网。为了解决这个问题，我们需要隔离广播域，

​            ---路由器：可以隔离广播域，并且每个接口都是属于不同的网段

​            --交换机 ：vlan（virtual local area network:虚拟局域网）

 当我们在交换机上使用了vlan 技术以后，在交换机的每个端口上收到的数据帧，最终都是属于某个指定的 vlan. 不同的 vlan之间，是不能通信的。并且，默认情况下，交换机所有的端口，都属于同一个vlan 1.

```java
<Huawei>display vlan                     //查看VLAN接口信息
The total number of vlans is : 1
--------------------------------------------------------------------------------
U: Up;         D: Down;         TG: Tagged;         UT: Untagged;
MP: Vlan-mapping;               ST: Vlan-stacking;
#: ProtocolTransparent-vlan;    *: Management-vlan;
--------------------------------------------------------------------------------

VID  Type    Ports                                                          
--------------------------------------------------------------------------------

1    common  UT:GE0/0/1(U)      GE0/0/2(U)      GE0/0/3(U)      GE0/0/4(D)      
                GE0/0/5(D)      GE0/0/6(D)      GE0/0/7(D)      GE0/0/8(D)      
                GE0/0/9(D)      GE0/0/10(D)     GE0/0/11(D)     GE0/0/12(D)     
                GE0/0/13(D)     GE0/0/14(D)     GE0/0/15(D)     GE0/0/16(D)     

                GE0/0/17(D)     GE0/0/18(D)     GE0/0/19(D)     GE0/0/20(D)     

                GE0/0/21(D)     GE0/0/22(D)     GE0/0/23(D)     GE0/0/24(U)     


VID  Status  Property      MAC-LRN Statistics Description      
--------------------------------------------------------------------------------

1    enable  default       enable  disable    VLAN 0001                         
<Huawei>
```

#### 1-3-2：vlan 

​            创建   vlan   vlan batch    

​            查看    display vlan 

​           加入   interface g0/01 

​                      port link-type access

​                      port default vlan 2

![](../day03/images/vlan.png)

需求：

交换机1/交机机2 创建vlan 10 20 将pc1/pc2/pc6加入vlan 10 pc3/pc4/pc5加入vlan 20

实现相同vlan内的PC互通

交换机1配置如下：

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]vlan batch  10 20 
Info: This operation may take a few seconds. Please wait for a moment...done.
[Huawei]port-group group-member GigabitEthernet 0/0/1  GigabitEthernet 0/0/2
[Huawei-port-group]port link-type access 
[Huawei-GigabitEthernet0/0/1]port link-type access 
[Huawei-GigabitEthernet0/0/2]port link-type access 
[Huawei-port-group]port default vlan 10
[Huawei-GigabitEthernet0/0/1]port default vlan 10
[Huawei-GigabitEthernet0/0/2]port default vlan 10
[Huawei-port-group]q	
[Huawei]int GigabitEthernet 0/0/3
[Huawei-GigabitEthernet0/0/3]port link-type access 
[Huawei-GigabitEthernet0/0/3]port default vlan 20
[Huawei-GigabitEthernet0/0/3]q
[Huawei]sysname Switch01
[Switch01]int GigabitEthernet 0/0/24	
[Switch01-GigabitEthernet0/0/24]port link-type trunk 
[Switch01-GigabitEthernet0/0/24]port trunk allow-pass vlan all
[Switch01-GigabitEthernet0/0/24]quit
[Switch01]
```

交换机上任何一个端口，在VLAN 中的类型为：

access(访问): 交换机上，连接非交 换机的端口，都设置为 access ,同一个时刻，只能属于同一个vlan   

trunk(干道)： 交换机上，连接交换机的端口，都设置为trunk

#### 1-3-3：pvid

![](../day03/images/vlan.png)

​     交换机 vlan 下的可隔离广播域，是因为终端的电信号进入交换机中被封闭为数据帧，在打上相应的 vlan标签，如图，pc1/pc6通信，pc1的电信号由交换机1的g0/0/1进入，g0/0/1口属于vlan10,交换机将pc1的电信号转化为数据帧为在打上vlan10的标签，从g0/0/24口出去，g0/0/24口允许所以 vlan 数据通过,默认pvid 1。g0/0/24口默认pvid1,刚保持vlan 10的标签来到了交换机2的g0/0/24口，g0/0/24口也是一样，同理，在交换机2广播 vlan 10 数据帧，被PC6接收。然后数据包返回，原理回来了PC1.一去一回正常通讯。

​     **如果两数据的 pvid 值相同，则untag转发，如果 pvid 值 不同，则保持tag转发**

​     pc1如果想和pc4通信。那么，pc1在从交换机1 g0/0/24 出口时，由于pvid 不同，而保持 vlan10 的标签进行转发。到了交换机2的g0/0/24口，由于要和pc4通信，而pc4属于vlan20 其pvid为20 所以我们在交换机2的g0/0/24口修改 pvid 为 20

```java
[Switch02-GigabitEthernet0/0/24]display this    //查看当时接口的所有配置
#
interface GigabitEthernet0/0/24
 port link-type trunk
 port trunk allow-pass vlan 2 to 4094
#
return
[Switch02-GigabitEthernet0/0/24]port trunk pvid vlan 20    //增加PVID VLAN 20标签
```

当pc4收到数据包进行回包时，进入交换机1的 g0/0/24口时，为了能和pc1 vlan10 进行通讯，所以要把他现在的标签 pvid 20（tag）更改为 pvid 10 命令如下：

```java
[Switch01-GigabitEthernet0/0/24]display this   //查看当时接口的所有配置
#
interface GigabitEthernet0/0/24
 port link-type trunk
 port trunk allow-pass vlan 2 to 4094
#
return
[Switch01-GigabitEthernet0/0/24]port trunk pvid vlan 10  ////增加PVID VLAN 10标签
```

测试：

```java
PC>ping 192.168.0.4                                  //不同VALN 的通信

Ping 192.168.0.4: 32 data bytes, Press Ctrl_C to break
From 192.168.0.4: bytes=32 seq=1 ttl=128 time=63 ms
From 192.168.0.4: bytes=32 seq=2 ttl=128 time=47 ms
From 192.168.0.4: bytes=32 seq=3 ttl=128 time=31 ms
From 192.168.0.4: bytes=32 seq=4 ttl=128 time=62 ms
From 192.168.0.4: bytes=32 seq=5 ttl=128 time=78 ms

--- 192.168.0.4 ping statistics ---
  5 packet(s) transmitted
  5 packet(s) received
  0.00% packet loss
  round-trip min/avg/max = 31/56/78 ms
```

课堂练习 pvid

<img src="../day03/images/pvid.png" style="zoom:80%;" />

交换机1/2/3  (notepad)

```java
undo terminal monitor
system-view
vlan batch 10 60
sysname Switch01/02/03
```

交换机1

```java
<Huawei>undo terminal monitor
Info: Current terminal monitor is off.
<Huawei>system-view
Enter system view, return user view with Ctrl+Z.
[Huawei]vlan batch 10 60
Info: This operation may take a few seconds. Please wait for a moment...done.
[Huawei]sysname Switch01
[Switch01]int GigabitEthernet 0/0/1	
[Switch01-GigabitEthernet0/0/1]port link-type access 
[Switch01-GigabitEthernet0/0/1]port default vlan 10	
[Switch01-GigabitEthernet0/0/24]port link-type trunk 	
[Switch01-GigabitEthernet0/0/24]port trunk allow-pass vlan all
[Switch01-GigabitEthernet0/0/24]
```

交换机2

```java
<Huawei>undo terminal monitor
Info: Current terminal monitor is off.
<Huawei>system-view
Enter system view, return user view with Ctrl+Z.
[Huawei]vlan batch 10 60
Info: This operation may take a few seconds. Please wait for a moment...done.
[Huawei]sysname Switch02
[Switch02]port-group group-member GigabitEthernet 0/0/23 GigabitEthernet 0/0/24	
[Switch02-port-group]port link-type trunk 
[Switch02-GigabitEthernet0/0/23]port link-type trunk 
[Switch02-GigabitEthernet0/0/24]port link-type trunk 	
[Switch02-port-group]port trunk allow-pass vlan all
[Switch02-GigabitEthernet0/0/23]port trunk allow-pass vlan all
[Switch02-GigabitEthernet0/0/24]port trunk allow-pass vlan all
```

交换机3

```
<Huawei>undo terminal monitor
Info: Current terminal monitor is off.
<Huawei>system-view
Enter system view, return user view with Ctrl+Z.
[Huawei]vlan batch 10 60
Info: This operation may take a few seconds. Please wait for a moment...done.
[Huawei]sysname Switch03
[Switch03-GigabitEthernet0/0/2]port link-type access 
[Switch03-GigabitEthernet0/0/2]port default vlan 60	
[Switch03-GigabitEthernet0/0/23]port link-type trunk 	
[Switch03-GigabitEthernet0/0/23]port trunk allow-pass vlan all
```

交换机1 设置

```
[Switch01-GigabitEthernet0/0/24]port trunk pvid vlan 10
[Switch01-GigabitEthernet0/0/24]
```

交换机3 设置

```
[Switch03-GigabitEthernet0/0/23]port trunk pvid vlan 60
[Switch03-GigabitEthernet0/0/23]
```

pc1 ping pc6

```java
PC>ping 192.168.10.16

Ping 192.168.10.16: 32 data bytes, Press Ctrl_C to break
From 192.168.10.16: bytes=32 seq=1 ttl=128 time=140 ms
From 192.168.10.16: bytes=32 seq=2 ttl=128 time=79 ms
From 192.168.10.16: bytes=32 seq=3 ttl=128 time=93 ms
From 192.168.10.16: bytes=32 seq=4 ttl=128 time=110 ms
From 192.168.10.16: bytes=32 seq=5 ttl=128 time=78 ms

--- 192.168.10.16 ping statistics ---
  5 packet(s) transmitted
  5 packet(s) received
  0.00% packet loss
  round-trip min/avg/max = 78/100/140 ms
```

查看命令

display port vlan

display this

#### 1-3-4: hybird

可以灵活控制端口上发送数据是否携带标签

可以像 access 端口一样，发送出的数据不带标签

可以像 trunk 端口一样，发送出去的数据携带标签

![](../day03/images/hybrid.png)

```java
<Huawei>undo terminal monitor
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]vlan 10
[Huawei]int GigabitEthernet 0/0/1
[Huawei-GigabitEthernet0/0/1]port link-type hybrid 
[Huawei-GigabitEthernet0/0/1]port hybrid pvid vlan 10

[Huawei-GigabitEthernet0/0/1]int g0/0/2	
[Huawei-GigabitEthernet0/0/2]port link-type hybrid 	
[Huawei-GigabitEthernet0/0/2]port hybrid untagged vlan 10	
[Huawei-GigabitEthernet0/0/2]port hybrid pvid vlan 10

[Huawei-GigabitEthernet0/0/2]int g0/0/1	
[Huawei-GigabitEthernet0/0/1]port hybrid untagged vlan 10
```

PC1 ping PC2

```JAVA
PC>ping 192.168.10.12

Ping 192.168.10.12: 32 data bytes, Press Ctrl_C to break
From 192.168.10.12: bytes=32 seq=1 ttl=128 time=46 ms
From 192.168.10.12: bytes=32 seq=2 ttl=128 time=32 ms

--- 192.168.10.12 ping statistics ---
  2 packet(s) transmitted
  2 packet(s) received
  0.00% packet loss
  round-trip min/avg/max = 32/39/46 ms
```

1-3-5：混合模式

![](../day03/images/Access.Trunk.Hybrid.png)

交换机1配置：

```java
<Huawei>undo terminal monitor
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch01
[Switch01]vlan batch 10 20 50
Info: This operation may take a few seconds. Please wait for a moment...done.	
[Switch01-GigabitEthernet0/0/1]port link-type hybrid 	
[Switch01-GigabitEthernet0/0/1]port hybrid pvid vlan 10	
[Switch01-GigabitEthernet0/0/24]port hybrid tagged vlan 10 20 50
[Switch01-GigabitEthernet0/0/1]port hybrid untagged vlan 10 50
```

交换机2配置：

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch02
[Switch02-GigabitEthernet0/0/24]port link-type hybrid 	
[Switch02-GigabitEthernet0/0/24]port hybrid tagged vlan 10 20 50
[Switch02-GigabitEthernet0/0/24]int g0/0/1
[Switch02-GigabitEthernet0/0/1]port link-type hybrid 
[Switch02-GigabitEthernet0/0/1]port hybrid untagged vlan 10 50
[Switch02-GigabitEthernet0/0/1]port hybrid pvid vlan 50
Error: The VLAN does not exist.
[Switch02-GigabitEthernet0/0/1]q	
[Switch02]vlan batch 10 20 50
Info: This operation may take a few seconds. Please wait for a moment...done.
[Switch02]int GigabitEthernet 0/0/1	
[Switch02-GigabitEthernet0/0/1]port hybrid pvid vlan 50
```

PC1 ping server

```java
PC>ping 192.168.10.50

Ping 192.168.10.50: 32 data bytes, Press Ctrl_C to break
From 192.168.10.50: bytes=32 seq=1 ttl=255 time=47 ms
From 192.168.10.50: bytes=32 seq=2 ttl=255 time=31 ms
From 192.168.10.50: bytes=32 seq=3 ttl=255 time=31 ms
From 192.168.10.50: bytes=32 seq=4 ttl=255 time=47 ms
From 192.168.10.50: bytes=32 seq=5 ttl=255 time=47 ms

--- 192.168.10.50 ping statistics ---
  5 packet(s) transmitted
  5 packet(s) received
  0.00% packet loss
  round-trip min/avg/max = 31/40/47 ms
```

配置思路：

​    整个网络中，通过分析总共有三个vlan 所以我们在两个交换机中创建三个vlan 10 20 50，从数据帧的收发顺序来进来配置。pc1要和server通信，pc1属于vlan10

所以我们在Switch01 的入口g0/0/1口配置 [Switch01]port hybrid pvid vlan 10 数据帧在交换机g0/0/24出 24口连接pc1 pc2 server,g0/0/24口要允许vlan 10 20 50 的数据通过，故[Switch01]port hybrid tagged vlan 10 20 50 ,交换机2的 g0/0/24口同理 [Switch02]port hybrid tagged vlan 10 20 50,当数据帧从交换机2的1口出来。将要脱掉vlan 10数据的标签，为了和server通信，他还要脱掉vlan 50 的数据标签。这个，server就能收到vlan 10数据，但网络的通信是一发一收，这样，vlan10的数据还要返回。在交换机2口 server属于vlan 50 所以：[Switch02-GitableEthernet0/0/1]port hybrid pvid vlan 50 ,在交换机2的24口出去，前面我们已经给他加上允许vlan 50 的数据通过。交换机1也同理。这样来到了交换机1口的1口。在这里，我们将vlan 50 和 vlan 10的标签脱掉，这样就完回了一次数据转发！

pc2至 server 自测
