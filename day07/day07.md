# Day07

## 1：dhcp产生的原因   

在现在的企业网络中，有大量的主机或设备需要获取IP地址等网络参数

   如果采用手工配置，工作量大，容易出错且不好管理

   如果用户擅自更改，还有可能会造成IP地址冲突等问题，使用动态主机配置协议DHCP，来分配地址等网络参数，可以减少管理员的工作量，避免出错。

   dhcp 工作原理

​    1：发现阶段：客户端广播 dhcp discover 报文

​    2：提供阶段：服务器回应 dhcp offer 报文

​     3：选择阶段：客户端广播发送 dhcp request报文

​     4：确认阶段：服务器回应 dhcp ack 报文 

![](../day07/images/dhcp.png)

### 1-1：dhcp 服务器配置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysn	
[Dhcop]sysname Dhcp	
[Dhcp]dhcp enable 
Info: The operation may take a few seconds. Please wait for a moment.done.
[Dhcp]ip pool vlan1
Info:It's successful to create an IP address pool.
[Dhcp-ip-pool-vlan1]network 192.168.1.0 mask 24
[Dhcp-ip-pool-vlan1]gateway-list 192.168.1.254
[Dhcp-ip-pool-vlan1]lease day 7                //租期 7天 默认 1天
[Dhcp-ip-pool-vlan1]dns-list 8.8.8.8
[Dhcp-ip-pool-vlan1]dns-list 114.114.114.114
[Dhcp-ip-pool-vlan1]q
[Dhcp]int GigabitEthernet0/0/2
[Dhcp-GigabitEthernet0/0/2]ip add 192.168.1.254 24
[Dhcp-GigabitEthernet0/0/2]dhcp sel	
[Dhcp-GigabitEthernet0/0/2]dhcp select go	
[Dhcp-GigabitEthernet0/0/2]dhcp select g	
[Dhcp-GigabitEthernet0/0/2]dhcp select global 
[Dhcp-GigabitEthernet0/0/2]
```

### 1-2：pc终端设置

![](../day07/images/pc.png)

### 1-3：保留一些IP地址

```java
[Dhcp-ip-pool-vlan1]excluded-ip-address 192.168.1.250 192.168.1.253
Warning:Some of addresses not be excluded are not idle,or not in the pool.
```

### 1-4: pc客户端

```shell
PC>ipconfig /release

IP Configuration


Link local IPv6 address...........: fe80::5689:98ff:fefd:1d14
IPv6 address......................: :: / 128
IPv6 gateway......................: ::
IPv4 address......................: 0.0.0.0
Subnet mask.......................: 0.0.0.0
Gateway...........................: 0.0.0.0
Physical address..................: 54-89-98-FD-1D-14
DNS server........................:

PC>ipconfig /renew

IP Configuration


Link local IPv6 address...........: fe80::5689:98ff:fefd:1d14
IPv6 address......................: :: / 128
IPv6 gateway......................: ::
IPv4 address......................: 192.168.1.249
Subnet mask.......................: 255.255.255.0
Gateway...........................: 192.168.1.254
Physical address..................: 54-89-98-FD-1D-14
DNS server........................: 8.8.8.8
                                    114.114.114.114
```

### 1-5: 重启 dhcp 地址池

```java
<Dhcp>reset ip pool name vlan1 all  
Warning: If the IP addresses that are being used are reclaimed, may influence no
rmal user in the network. Are you sure to continue?[Y/N]:y
```

### 1-6: 分配固定地址

```java
[Dhcp-ip-pool-vlan1]static-bind ip-address 192.168.1.111 mac-address 5489-98FD-1D14  //重启地址池，释放IP，设置static-bind,重新获取
```

## 2：DHCP 中继

![](../day07/images/dhcprelay.png)

### 2-1: DHCP服务器配置

```java
<Huawei>
Nov 25 2021 14:21:06-08:00 Huawei %%01IFPDT/4/IF_STATE(l)[0]:Interface GigabitEt
hernet0/0/0 has turned into UP state.

<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Dhcp	
[Dhcp]dhcp enable 
Info: The operation may take a few seconds. Please wait for a moment.done.
[Dhcp]ip pool ruhua
Info: It's successful to create an IP address pool.
[Dhcp-ip-pool-ruhua]network 192.168.100.0 mask 24
[Dhcp-ip-pool-ruhua]gateway-list 192.168.100.254 
[Dhcp-ip-pool-ruhua]dns-list 61.134.1.4 218.30.19.40
[Dhcp-ip-pool-ruhua]q
[Dhcp]int GigabitEthernet 0/0/0
[Dhcp-GigabitEthernet0/0/0]ip add 10.10.10.1 24	
[Dhcp-GigabitEthernet0/0/0]dhcp select global 

  Please check whether system data has been changed, and save data in time

  Configuration console time out, please press any key to log on

	
<Dhcp>undo terminal monitor 
Info: Current terminal monitor is off.
<Dhcp>sys	
<Dhcp>system-view 
Enter system view, return user view with Ctrl+Z
[Dhcp]display ip routing-table 192.168.100.0	
[Dhcp]ip route-static 192.168.100.0 24 10.10.10.2

```

### 2-2: DHCP中继

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname dhcp relay
[dhcp relay]int GigabitEthernet 0/0/1
[dhcp relay-GigabitEthernet0/0/1]ip add 10.10.10.2 24	
[dhcp relay-GigabitEthernet0/0/1]int g0/0/2
[dhcp relay-GigabitEthernet0/0/2]ip add 192.168.100.254 24
[dhcp relay-GigabitEthernet0/0/2]dhcp select relay 
Error: Please enable DHCP in the global view first.
[dhcp relay-GigabitEthernet0/0/2]q
[dhcp relay]dhcp enable 
Info: The operation may take a few seconds. Please wait for a moment.done.
[dhcp relay]int GigabitEthernet 0/0/2
[dhcp relay-GigabitEthernet0/0/2]dhcp select relay 
[dhcp relay-GigabitEthernet0/0/2]dhcp relay server-ip 10.10.10.1
[dhcp relay-GigabitEthernet0/0/2]q
[dhcp relay]display ip routing-table 10.10.10.1
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Table : Public
Summary Count : 1
Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

     10.10.10.0/24  Direct  0    0           D   10.10.10.2      GigabitEthernet
0/0/1

[dhcp relay]

  Please check whether system data has been changed, and save data in time

  Configuration console time out, please press any key to log on

```

### 2-3: 客户端获取IP地址

```shell
PC>ipconfig /renew

IP Configuration


Link local IPv6 address...........: fe80::5689:98ff:fe8f:3875
IPv6 address......................: :: / 128
IPv6 gateway......................: ::
IPv4 address......................: 192.168.100.253
Subnet mask.......................: 255.255.255.0
Gateway...........................: 192.168.100.254
Physical address..................: 54-89-98-8F-38-75
DNS server........................: 61.134.1.4
                                    218.30.19.40

```

### 2-4: 释放IP 地址

```shell
PC>ipconfig /release

IP Configuration


Link local IPv6 address...........: fe80::5689:98ff:fe8f:3875
IPv6 address......................: :: / 128
IPv6 gateway......................: ::
IPv4 address......................: 0.0.0.0
Subnet mask.......................: 0.0.0.0
Gateway...........................: 0.0.0.0
Physical address..................: 54-89-98-8F-38-75
DNS server........................:
```

## 3:  Vlan之间Dhcp服务器的设置

![](../day07/images/vlanordhcp.png)

1-1: PC终端设置（pc1-pc4）

![](../day07/images/pc.png)                                      

1-2: 交换机 2 设置

```java
The device is running!
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch2
[Switch2]vlan batch 10 20
Info: This operation may take a few seconds. Please wait for a moment...done.	
[Switch2]port-group group-member Ethernet 0/0/1 Ethernet 0/0/2	
[Switch2-port-group]port link-type access 
[Switch2-Ethernet0/0/1]port link-type access 
[Switch2-Ethernet0/0/2]port link-type access 
[Switch2-port-group]PORT default vlan 10
[Switch2-Ethernet0/0/1]PORT default vlan 10
[Switch2-Ethernet0/0/2]PORT default vlan 10
[Switch2-port-group]q
[Switch2]int Eth0/0/3	
[Switch2-Ethernet0/0/3]port link-type trunk 
[Switch2-Ethernet0/0/3]port trunk allow-pass vlan all

```

1-3: 交换机 3 设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch2
[Switch2]vlan batch 10 20
Info: This operation may take a few seconds. Please wait for a moment...done.	
[Switch2]port-group group-member Ethernet 0/0/1 Ethernet 0/0/2	
[Switch2-port-group]port link-type access 
[Switch2-Ethernet0/0/1]port link-type access 
[Switch2-Ethernet0/0/2]port link-type access 
[Switch2-port-group]PORT default vlan 10
[Switch2-Ethernet0/0/1]PORT default vlan 10
[Switch2-Ethernet0/0/2]PORT default vlan 10
[Switch2-port-group]q
[Switch2]int Eth0/0/3	
[Switch2-Ethernet0/0/3]port link-type trunk 	
[Switch2-Ethernet0/0/3]port trunk allow-pass vlan all
```

1-4: 交换机 1  设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch1	
[Switch1]dhcp enable 
Info: The operation may take a few seconds. Please wait for a moment.done.
[Switch1]vlan batch 10 20
Info: This operation may take a few seconds. Please wait for a moment...done.
[Switch1]ip pool vlan10
Info:It's successful to create an IP address pool.	
[Switch1-ip-pool-vlan10]network 192.168.10.0 mask 24
[Switch1-ip-pool-vlan10]gateway-list 192.168.10.254
[Switch1-ip-pool-vlan10]dns-list 61.134.1.4 218.30.19.40
[Switch1]ip pool vlan20
Info:It's successful to create an IP address pool.	
[Switch1-ip-pool-vlan20]network 192.168.20.0 mask 24
[Switch1-ip-pool-vlan20]gateway-list 192.168.20.254
[Switch1-ip-pool-vlan20]dns-list 8.8.8.8 114.114.114.114
[Switch1-ip-pool-vlan20]q	
[Switch1]int Vlanif 10
[Switch1-Vlanif10]ip add 192.168.10.254 24
[Switch1-Vlanif10]dhcp select global 
[Switch1-Vlanif10]q
[Switch1]int Vlanif 20
[Switch1-Vlanif20]ip add 192.168.20.254 24	
[Switch1-Vlanif20]dhcp select global 
[Switch1-Vlanif20]quit	
[Switch1]port-group group-member GigabitEthernet 0/0/1 GigabitEthernet 0/0/2	
[Switch1-port-group]port link-type trunk 
[Switch1-GigabitEthernet0/0/1]port link-type trunk 
[Switch1-GigabitEthernet0/0/2]port link-type trunk 	
[Switch1-port-group]port trunk allow-pass vlan all
[Switch1-GigabitEthernet0/0/1]port trunk allow-pass vlan all
[Switch1-GigabitEthernet0/0/2]port trunk allow-pass vlan all
```

1-5: 验证

```java

PC1>ipconfig

Link local IPv6 address...........: fe80::5689:98ff:fe22:1793
IPv6 address......................: :: / 128
IPv6 gateway......................: ::
IPv4 address......................: 192.168.10.253
Subnet mask.......................: 255.255.255.0
Gateway...........................: 192.168.10.254
Physical address..................: 54-89-98-22-17-93
DNS server........................: 61.134.1.4
                                    218.30.19.40
Welcome to use PC Simulator!

PC3>ipconfig

Link local IPv6 address...........: fe80::5689:98ff:fea5:252b
IPv6 address......................: :: / 128
IPv6 gateway......................: ::
IPv4 address......................: 192.168.20.253
Subnet mask.......................: 255.255.255.0
Gateway...........................: 192.168.20.254
Physical address..................: 54-89-98-A5-25-2B
DNS server........................: 8.8.8.8
                                    114.114.114.114

```





