# Day02

## 1:TCP/IP协议簇

​    TCP/IP协议不仅仅是TCP和IP两个协议，而是指一个由FTP.SMTP.TCP.UCP.IP等不同的协议构成的协议簇，只是因为在TCP/IP协议中TCP协议和IP协议最具有代表性，所以被称为TCP/IP协议。

​    TCP/IP传输协议，即传输控制/网络协议，也叫作网络通讯协议。它是在网络中使用的最基本的通信协议。TCP/IP传输协议对互联网中各部分进行通信的标准和方法进行了规定。并且，TCP/IP传输协议是保证网络信息及时，完整传输的两个重要的协议。TCP/IP传输协议严格来说是一个四层体系结构，应用层、传输层，网络层，数据链路层都包含其中。

### 1-1：OSI网络七层模型

​         OSI网络七层模型，是国际标准化组织（ISO）制定的一个用于计算机或通信系统间互联的标准体系，一般称为OSI参考模型或七层模型。

####          1-1-1：物理层

​         建立、维护、断开物理连接（由底层网络定义的协议）

####          1-1-2：数据链路层

​         建立逻辑连接，进行硬件地址寻址，差错校难等功能 （MAC地址）

####         1-1-3：网络层

​        进行逻辑地址寻址，实现不同网络之间的路径选择

​        协议有：ICMP IGMP IP(IPV4 IPV6)

####        1-1-4：传输层

​       定义传输数据的协议端口号，以及流控和差错校验

​       协议有：TCP UDP,数据包一旦离开网卡即进入网络传输层

####         1-1-5：会话层

​       建立、管理、终止会话。（五层模型里面已经合并到了应用层）

​       对应主机进程，指本地主机与远程主机下在进行的会话

####          1-1-6：表示层

​       数据的表示、安全、压缩。（五层模型里面已经合并到了应用层）

​       格式有 JPEG、ASCII、EBCDIC、加密格式等

####         1-1-7：应用层

​      网络服务与最终用户的一个接口。

​      协议有：HTTP FTP TFTP SMTP SNMP DNS TELNET HTTPS POP3 DHCP

​     总结说人话： 1：TCP主要负责如何传，用什么协议传，传输的方法

​                            2：IP主要负责传输的目的地是那里，传给谁。

### 1-2：IP地址

​      用来标识一个网络节点的互联网地址（如同一个人的身份证号码）。

####      1-2-1：IP地址的组成和数量

​     地址长度是 32位 是4个十进制（0-255）表示，以 . 隔开

​    0.0.0.0-255.255.255.255

​    IPv4的IP总数量为：2^32=42亿个地址 ，由于互联网的发展，网络中的节点越来越来。IPv4地址将面临枯竭，所以我们伟大的科学机有发明了IPv6,在IPv6问世       时，科学号说，IPv6将保证我们地球上的每一粒沙子都会拥有一个IP地址。

​     地址数为：2^128=3.4*10^38    16进制  

​     IPv6的地址长度为128bit，格式是8组4位的16进制数，例如:

​     ABCD:EF01:2345:6789:ABCD:EF01:2345:6789

​     IPv4的地址长度为32bit,格式是4组8位的2进进制数。例如

​     11111111:11111111:11111111:11111111

####        1-2-2：IP地址的分类

​     根据类别区别： IP地址分A、B、C、D、E四大类  根据使用范围区分：公有地址：可以在互联网合法使用，但需要向NIC付费申请，全球唯一;私有地址：预留给企业内部使用，无需付费，局域网唯一。

​      类别                IP范围                            默认子网掩码              私有IP范围                                    网络位主机位

​      A类    0.0.0.0   - 127.255.255.255        255.0.0.0          10.0.0.0-10.255.255.255                       网、主、主、主

​      B类   128.0.0.0 - 191.255.255.255       255.255.0.0       172.16.0.0-172.31.255.255                  网、网、主、主

​     C类    192.0.0.0 - 223.255.255.255        255.255.255.0   192.168.0.0-192.168.255.255             网、网、网、主

​     D类    224.0.0.0 - 239.255.255.255        组播

​     E类    240.0.0.0 - 254.255.255.255        科研

#### 1-2-3 特殊地址和保留地址

​      0.0.0.0

​     严格意义上来说，他已经不是一个IP地址了，他表示是的是这样一个集合，他代表网络中不知道网络位和主机位，也可以把他看成一个最小的IP

​     255.255.255.255

​      限制广播地址，对本机来说，这个地址指本网段内（同一个广播域）所有的主机

​     127.x.x.x

​     是个保留地址，用做循环测试的

​    169.254.x.x

​     这是微软的一个保留地址。

#### 1-2-4 操作系统下查看ip地址

​        windows 点击运行窗口，在运行窗口中输入cmd,进入ms-dos窗口，在提示符中键入ipconfig 就能看到IP信息，如果想要看得更加全面，可输入ipconfig/all

![](../day02/images/win-ipconfig.png)

​     

​       Linux查看IP信息命令是：ifconfig 或 ifconfig | grep netmask

<img src="../day02/images/Linux-ifconfig.png" style="zoom: 80%;" />

<img src="C:\Users\Tedu\Desktop\榆林学院-运维\day02\images\Linux-ifconfig2.png" style="zoom:65%;" />

## 2:同网段PC终端相互通信

###       2-1：绘制网络拓扑图，设置IP地址为同一网段

![](../day02/images/nodifferent.png)

### 2-2：华为交换机的基本设置

​     1:<Huawei>undo terminal monitor                  关闭消息日志提醒

```java
<Huawei>undo terminal monitor          //  关闭消息日志提醒
Info: Current terminal monitor is off.
<Huawei>
```

​     2:<Huawei>language-mode Chinese               切换语言模式

```java
<Huawei>language-mode Chinese          //  切换语言模式
Change language mode, confirm? [Y/N] y
提示：改变语言模式成功。
```

​     3:<Huawei>system-view                                用户视图进入系统视图     

```java
<Huawei>system-view                    //用户视图进入系统视图
Enter system view, return user view with Ctrl+Z.
[Huawei]
```

​    4:[Huawei]sysname Tedu                                 修改设备名为 Tedu

```java
[Huawei]sysname Tedu                  //修改设备名为 Tedu   
[Tedu]
```

   5:[Tedu]interface Vlanif 1                                  进入Vlan 1

```java
[Tedu]interface Vlanif 1              //进入Vlan 1
[Tedu-Vlanif1]
```

  6：[Tedu-Vlanif1]ip address  192.168.0.254 24 交换机设置IP地址为192.168.0.254/24

```java
[Tedu-Vlanif1]ip address  192.168.0.254 24   //设置交换机IP地址为192.168.0.254、子网掩码为255.255.255.0
[Tedu-Vlanif1]
```

### 2-3：ping 命令测试

<img src="../day02/images/ping.png" style="zoom: 80%;" />

## 3:交换机设置密码

###  3-1: password密码验证

```java
[Tedu]user-interface console 0              //进入console 0接口	
[Tedu-ui-console0]idle-timeout 0            //设置退出时间为 0 永不退出
[Tedu-ui-console0]

[tedu]user-interface console 0
[tedu-ui-console0]authentication-mode password //启用密码验证模式	
[tedu-ui-console0]set authentication password ?
  cipher  Set the password with cipher text    //密文
  simple  Set the password in plain text       //明文
	
[tedu-ui-console0]set authentication password cipher shutong8511   //设置登录密码为 shutong8511
[tedu-ui-console0]re	
[tedu-ui-console0]return
<tedu>quit User interface con0 is available



Please Press ENTER.


Login authentication


Password:
```

### 3-2：AAA验证模式

```java
<Huawei>undo terminal monitor                    //关闭消息提醒日志
Info: Current terminal monitor is off.
<Huawei>system-view                              //进入系统视图
Enter system view, return user view with Ctrl+Z. 
[Huawei]aaa                                      //进入AAA设置
[Huawei-aaa]local-user admin password cipher shutong8511   //设置本地新建用户admin密码shutong8511
[Huawei-aaa]local-user admin service-type terminal         //赋予admin用户有终登录权限
[Huawei-aaa]quit
[Huawei]user-interface console 0	                      //进入控制接口 0 
[Huawei-ui-console0]authentication-mode aaa                //启用AAA验证模式
[Huawei-ui-console0]q
[Huawei]q
<Huawei>q User interface con0 is available



Please Press ENTER.


Login authentication


Username:admin             //验证
Password:                  //Nice
<Huawei>
```

## 4：交换机Telnet远程登录

###      4-1：网络连通性的设置与测试

![](../day02/images/telnet.png)

交换机YuLin的基本、网络设置：

```java
<Huawei>undo terminal monitor
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.	
[Huawei]sysname YuLin
[YuLin]int Vlanif 1                              //进入虚拟VLAN 1接口
[YuLin-Vlanif1]ip add 192.168.0.1 255.255.255.0  //设置VLAN 1接口IP地址
[YuLin-Vlanif1]quit	
[YuLin]display ip interface brief                //查看系统IP地址和端口状态
*down: administratively down
^down: standby
(l): loopback
(s): spoofing
The number of interface that is UP in Physical is 2
The number of interface that is DOWN in Physical is 1
The number of interface that is UP in Protocol is 2
The number of interface that is DOWN in Protocol is 1

Interface                         IP Address/Mask      Physical   Protocol  
MEth0/0/1                         unassigned           down       down      
NULL0                             unassigned           up         up(s)     
Vlanif1                           192.168.0.1/24       up         up        
[YuLin]
```

交换机HongKong的基本、网络设置

```java
The device is running!

<Huawei>undo terminal monitor
Info: Current terminal monitor is off.
<Huawei>system-view
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname HongKong
[HongKong]interface vlan 1
[HongKong-Vlanif1]ip address 192.168.0.2 24	
[HongKong-Vlanif1]display ip interface brief 
*down: administratively down
^down: standby
(l): loopback
(s): spoofing
The number of interface that is UP in Physical is 2
The number of interface that is DOWN in Physical is 1
The number of interface that is UP in Protocol is 2
The number of interface that is DOWN in Protocol is 1

Interface                         IP Address/Mask      Physical   Protocol  
MEth0/0/1                         unassigned           down       down      
NULL0                             unassigned           up         up(s)     
Vlanif1                           192.168.0.2/24       up         up        
[HongKong-Vlanif1]
```

测试网络的连通性

```java
[HongKong]ping -c 2 192.168.0.1                   //向目标地址发送2个数据包
  PING 192.168.0.1: 56  data bytes, press CTRL_C to break
    Reply from 192.168.0.1: bytes=56 Sequence=1 ttl=255 time=60 ms
    Reply from 192.168.0.1: bytes=56 Sequence=2 ttl=255 time=20 ms

  --- 192.168.0.1 ping statistics ---
    2 packet(s) transmitted
    2 packet(s) received
    0.00% packet loss
    round-trip min/avg/max = 20/40/60 ms

[HongKong]
```

### 4-2 交换机上设置用户并赋予telnet权限

 香港交换机上设置 telnet权限 

```java
[HongKong]user-interface vty 0 4	            //进入远程虚拟接口 vty 0 4
[HongKong-ui-vty0-4]set authentication password cipher tarena	 //设置Telnet远程密码密码文 tarena
[HongKong-ui-vty0-4]
```

测试 （榆林交换机上测试 telnet功能）

```java
<LuLin>telnet 192.168.0.2                      //telnet 默认端口为23 可不追加
Trying 192.168.0.2 ...
Press CTRL+K to abort
Connected to 192.168.0.2 ...


Login authentication


Password:
Info: The max number of VTY users is 5, and the number
      of current VTY users on line is 1.
      The current login time is 2021-10-29 14:58:14.
<HongKong>                                    //默认等级权限为 3（普通用户）
```

Telnet 增加权限

```java
[HongKong-ui-vty0-4]user privilege level 15   //赋予telnet权限为最高级
[HongKong-ui-vty0-4]
```

测试

```java
<LuLin>telnet 192.168.0.2
Trying 192.168.0.2 ...
Press CTRL+K to abort
Connected to 192.168.0.2 ...


Login authentication


Password:
Info: The max number of VTY users is 5, and the number
      of current VTY users on line is 1.
      The current login time is 2021-10-29 15:01:39.
<HongKong>sys	
<HongKong>system-view                          //测试成功
Enter system view, return user view with Ctrl+Z.
[HongKong]
```

修改 Telnet 端口

```java
[HongKong]telnet server port 6666             //修改Telnet 端口号为 6666
Warning: This operation will cause all the online Telnet users to be offline. Co
ntinue?[Y/N]:y                                //同意修改
Info: Succeeded in changing the listening port of the Telnet server.
[HongKong]
```

测试修改后的端口

```java
<LuLin>telnet 192.168.0.2 6666               //测试 6666 端口
Trying 192.168.0.2 ...
Press CTRL+K to abort
Connected to 192.168.0.2 ...


Login authentication


Password:
Info: The max number of VTY users is 5, and the number
      of current VTY users on line is 1.
      The current login time is 2021-10-29 15:07:24.
<HongKong>                                    //nice
```

自行测试 AAA 验证

```java
<HongKong>undo terminal monitor
Info: Current terminal monitor is off.
<HongKong>system-view 
Enter system view, return user view with Ctrl+Z.
[HongKong]aaa                                         //进入AAA模式
[HongKong-aaa]local-user admin service-type telnet 	  //新建本地用户admin给此用户赋予Telnet权限
[HongKong-aaa]local-user admin password cipher 123    //设置admin用户密文密码123
[HongKong-aaa]local-user admin privilege level 3      //设置admin用户等级为3
[HongKong-aaa]local-user admin privilege level 1      //设置admin用户等级为1 
[HongKong-aaa]q
[HongKong]user-interface vty 0 3                      //进入虚拟终端3 
[HongKong-ui-vty0-3]authentication-mode aaa           //在虚拟终端启用AAA验证模式

```

## 5: 交换机配置信息的备份和还原

![](../day02/images/update.png)

### 5-1：Backup的配置

```java
<Huawei>undo terminal monitor
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Backup	
[Backup]interface Vlanif 1
[Backup-Vlanif1]ip address 172.199.10.10 24
[Backup-Vlanif1]return
<Backup>save tarena.zip
Are you sure to save the configuration to flash:/tarena.zip?[Y/N]:y //确认 y
Now saving the current configuration to the slot 0.
Save the configuration successfully.
<Backup>dir
Directory of flash:/

  Idx  Attr     Size(Byte)  Date        Time       FileName 
    0  drw-              -  Aug 06 2015 21:26:42   src
    1  drw-              -  Oct 29 2021 14:08:07   compatible
    2  -rw-            476  Oct 29 2021 16:39:05   tarena.zip         //配置保存文件tarena.zip

32,004 KB total (31,968 KB free)

<Backup>
```

### 5-2:  update的配置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Update	
[Update]int Vlanif 1
[Update-Vlanif1]ip add 172.199.10.20 24
[Update-Vlanif1]
```

### 5-3：测试网络

```java
[Update]ping 172.199.10.10          //测试网络
  PING 172.199.10.10: 56  data bytes, press CTRL_C to break
    Reply from 172.199.10.10: bytes=56 Sequence=1 ttl=255 time=30 ms
    Reply from 172.199.10.10: bytes=56 Sequence=2 ttl=255 time=50 ms
    Reply from 172.199.10.10: bytes=56 Sequence=3 ttl=255 time=50 ms
    Reply from 172.199.10.10: bytes=56 Sequence=4 ttl=255 time=50 ms
    Reply from 172.199.10.10: bytes=56 Sequence=5 ttl=255 time=60 ms

  --- 172.199.10.10 ping statistics ---
    5 packet(s) transmitted
    5 packet(s) received
    0.00% packet loss
    round-trip min/avg/max = 30/48/60 ms
```

### 5-4：开启FTP功能，上传保存的配置

```java
<Update>mkdir xaliupeng             //创建一个文件夹 xaliupeng
Info: Create directory flash:/xaliupeng......Done.
<Update>dir                         //查看目录文件夹
Directory of flash:/

  Idx  Attr     Size(Byte)  Date        Time       FileName 
    0  drw-              -  Aug 06 2015 21:26:42   src
    1  drw-              -  Nov 01 2021 14:43:34   compatible
    2  drw-              -  Nov 01 2021 14:57:29   xaliupeng

32,004 KB total (31,968 KB free)

```

 AAA模式下配置FTP用户，密码，目录

```JAVA
[Update]aaa                      //AAA
[Update-aaa]local-user admin service-type ftp     //新建本地用户admin给此用户赋予Telnet权限
[Update-aaa]local-user admin password cipher 123  //设置admin用户密文密码123
[Update-aaa]local-user admin ftp-directory  flash:/xaliupeng/  //设置ftp主目录为flash:/xaliupeng/
[Update-aaa]q	                                               //退出
[Update]ftp server enable                                      //开启FTP功能
Info: Succeeded in starting the FTP server.

```

上传备份配置

```java
<Backup>ftp                                                   //测试 ftp
[ftp]open	                                                  //连接ftp open
[ftp]open 172.199.10.20
Trying 172.199.10.20 ...
Press CTRL+K to abort
Connected to 172.199.10.20.
220 FTP service ready.
User(172.199.10.20:(none)):admin                //键入设置的FTP帐号
331 Password required for admin.
Enter password:                                 // 密码
230 User logged in.

[ftp]dir                                         //查看目录
200 Port command okay.
150 Opening ASCII mode data connection for *.
226 Transfer complete.

[ftp]lcd                                         //切换本地目录
The current local directory is flash:.

[ftp]put tarena.zip                              //上传本地文件至远程FTP服务器的目录
200 Port command okay.
150 Opening ASCII mode data connection for tarena.zip.

100%     
226 Transfer complete.
FTP: 476 byte(s) sent in 0.310 second(s) 1.53Kbyte(s)/sec.
```

恢复备份的配置文件  tarena.zip

```java
<Huawei>dir
Directory of flash:/

  Idx  Attr     Size(Byte)  Date        Time       FileName 
    0  drw-              -  Aug 06 2015 21:26:42   src
    1  drw-              -  Nov 01 2021 16:13:05   compatible
    2  -rw-            476  Nov 01 2021 16:15:58   tarena.zip

32,004 KB total (31,968 KB free)

<Huawei>startup saved-configuration tarena.zip  //恢复配置的文件 tarena.zip
Info: Succeeded in setting the configuration for booting system.
<Huawei>
<Huawei>reboot                                  //重启
Info: The system is now comparing the configuration, please wait.
Warning: All the configuration will be saved to the configuration file for the n
ext startup:flash:/tarena.zip, Continue?[Y/N]:n  //N0
Info: If want to reboot with saving diagnostic information, input 'N' and then e
xecute 'reboot save diagnostic-information'.
System will reboot! Continue?[Y/N]:y    //yes
```

重启后：

```php
<Backup>      //Good luck!
```
