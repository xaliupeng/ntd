# Day04

## 1:  STP技术

  1-1：单点故障

​            PC之间互通的链路仅仅存在1个

​           任何一个设备链路出现的问题，pc之间都无法通信

​                                                  ![](../day04/images/stp1.png) 

​    解决方案：

​             增加冗余/备份设备

​             增加冗余/备份链路

![](../day04/images/stp2.png)

​      交换机正常的数据转发：

​      交换机是位于OSI参考模型中第2层（即数据链路层）的设备,可以识别数据包中的MAC地址信息,并根据MAC地址转发数据包,将这些MAC地址与对应的端口记录在自身内部的一个地址表中,其具体工作流程如下：

1）交换机从端口接收数据包,当数据包从某一个端口到达交换机时,交换机先读取数据包头部中的源MAC地址,从而获知源MAC地址的设备连在交换机的哪个端口上;

2）交换机读取数据包头部中的目的MAC地址,并在交换机中维护地址表,交换机从地址表中查找MAC地址对应的端口,从而获知应将数据包从哪个端口转发出去;

3）如果在地址表中有与目的MAC地址对应的端口,则交换机把数据包直接发送到该端口上完成数据传输;

4）如果在地址表中找不到对应的端口,交换机就把数据包广播到所有端口上,若目的设备收到广播包并作出回应,则交换机记录这一目的MAC地址与哪个端口对应,在下次传送数据时不再需要对所有端口进行广播。交换机不断循环此过程,即可学习到全网的MAC地址信息。

STP概述

STP -- spanning tree protocol(生成树协议)

逻辑上断开环路，防止广播风暴的产生

当线路故障，阻塞接口被激活;线路恢复，不转发数据，仅起备份作用



​      因为交换网络设备中间会存在单点故障，所以呢？我们就加了一个冗余链路，但是没有想到引起了一个新的问题，环路，导致了广播风暴，为了解决这个问题，我们就学习了STP技术。

单点故障：

​       两个设备之间通信的时候，只有一个链路。一旦链路上的任何一个设备或者线路出现了故障，都会导致设备之间无法互通。

​      解决方案：

​              增加冗余/备份的线路或者设备

新的问题：

​        如果交换机之间，同是连接多个线路，就会导致“环路”，从而形成广播风暴，导致的结果是：大量的数据不断的在交换机之间互相传输，导致设备的CPU利用率升高。严重的时候，导致设备重启/宕机  

解决方案：

​        在交换机之间，运行STP 协议，防止环路的发生

===================================================================

   stp (spanning tree protocol)

​    --生成树协议

​       主要作用是在”交换网络中“，防止环路的发生

原理：

​      1：在交换机之间，为每个交换机确定角色

​                 --根交换机   每个网段中，只有1个根交换机;

​                 --非根交换机  除了根交换机心外的其他所有交换机

​                --选举原则：

​                          STP在每个交换机上运行之后，都会为每个交换机起一个名子，叫做  （bridge id）.BID越小，越优先成为”根交换机“！

​                 BID=优先级（2字节）+ mac()6个字节）

​                     *优先级：默认是32768

​                      网桥优先级的取值范围=0--65535

​                      缺省值：32768

​                     *mac地址：交换机上的主板的MAC地址，全世界唯一；查看命令：display bridge mac-address

​      2：确定交换机的端口角色

​                  --根端口   在非根交换机上，有且只有1个，距离根交换机最近

​                  --指定端口  在每条链路上有且只有1个，距离根交换机最近

​                  --非指定端口  剩下的其他的有的接口，都叫做”非指定端口“

​      3：确定端口的状态

​                   --disable:禁用状态

​                   --listening：侦听状态

​                   --learning:    学习状态

​                    --forearding:转发状态（根端口/指定端口）

​                    --blocking: 阻塞状态（非指定端口）

## 2：STP 工作原理

​           stp工作原理分三个步骤：

​           1：选择根交换机（Root bridge）

​           2:  选择根端口    （Root Port）

​           3:  选择指定端口 （Designated Port）

## 3：stp 端口指定

![](../day04/images/stp3.jpg.png)

### 3-1：查看交换机的MAC地址

```java
<Huawei>display bridge mac-address 
System bridge MAC address: 4c1f-ccfa-3f6b
<Huawei>
```

### 3-2: 查看交换机STP 网桥优先级

```java
<Huawei>display stp
-------[CIST Global Info][Mode MSTP]-------
CIST Bridge         :32768.4c1f-ccfa-3f6b
Config Times        :Hello 2s MaxAge 20s FwDly 15s MaxHop 20
Active Times        :Hello 2s MaxAge 20s FwDly 15s MaxHop 20
CIST Root/ERPC      :32768.4c1f-cc19-2345 / 20000
CIST RegRoot/IRPC   :32768.4c1f-ccfa-3f6b / 0
```

### 3-3: 修改交换机的网络优先级

```java
[Huawei]stp priority 4096
[Huawei]dis	
[Huawei]display stp
-------[CIST Global Info][Mode MSTP]-------
CIST Bridge         :4096 .4c1f-ccfa-3f6b
Config Times        :Hello 2s MaxAge 20s FwDly 15s MaxHop 20
Active Times        :Hello 2s MaxAge 20s FwDly 15s MaxHop 20
CIST Root/ERPC      :4096 .4c1f-ccfa-3f6b / 0
CIST RegRoot/IRPC   :4096 .4c1f-ccfa-3f6b / 0
```

### 3-4:查看 stp 状态信息 

```java
<Huawei>display stp brief 
 MSTID  Port                        Role  STP State     Protection
   0    GigabitEthernet0/0/2        DESI  FORWARDING      NONE
   0    GigabitEthernet0/0/3        ROOT  FORWARDING      NONE
```

## 4: stp 数据流向选择和网络优化

<img src="../day04/images/display stp.png" style="zoom:80%;" />

如上图：pc3到pc4，由于stp技术，将会阻塞交换机A的 g0/0/1,但由于业务的正常需要，pc3到pc4的数据要经过交换机B，这个我们需要修改STP网桥的优先及BID的值 。从上述的实验证明，stp技术不仅起来了链路的冗余备份和网络优化的功能。

## 5:MSTP

   BPDU 

   根网桥ID

​    根路径成本

   发送网桥ID

 每两次发送HELLO 报文 最大老化时间20s .从丢弃到转到一共要持续30s,从发现问题，到解决问题，总共时间为50S

   STP-RSTP-MSTP

![](../day04/images/mstp.jpg.png)

命令：

[Switch]stp priority 0          //必须是4096的倍数  默认是32768

[Switch]stp mode mstp      //每个交换机的STP模式必须相同 华为交换机默认开启mstp

[Switch]display stp            //查看stp详细信息，模式为 stp

[Switch-GableEthernet0/0/12]

​              stp cost 4001      //将端口的开销值配置为40001 百兆2w 千兆是20w

[Switch]display stp interface gi0/0/x  //查看指定接口的STP信息

MSTP产生的背景：

​           随着网络 的发展，业务类型对底层传输的延迟要求越来越敏感，但是，stp的网络出现问题，从故障发现到故障解决中间需要的时间（收敛时间）很久，时间范围是30s-50s

STP的计时器：

-hello          ,默认时间为2s,表示的是周期性的发送BPDU的时间；

--max age  ,默认是20s,表示的是一个BPDU的最大存活时间

--forward delay ,默认是15s,表示的是接口在“侦听”和“学习”状态分别停留的时间。

所以，我们针对该问题，对STP协议，进行了升级，所以就有了RSTP,即快速生成树协议。通过一些改进，我们可以确保RSTP的收敛时间，可以达毫秒级别。

但是，RSTP依然存在问题，仅仅能够实现设备之间的“备份”，不能实现设备之间的“负载均衡”，导致设备的利用率太低。

所以，我们再次对RSTP进行升级，从而有了：mstp

MSTP：multiple stp 即多生成树协议。

指的是在交换机之间运行了mstp以后，整个交换网络中，不再是一棵树了，可以存在很多很多树。终极情况下，我们可以为每个vlan配置一根树。

我们可以通过为不同的VLAN配置不同的根交换机。从而可以确保整个交换网络中的设备和链路，得到充分的利用。

同时，这些设备和链路，依然能够实现“备份”功能。

MSTP配置思路：

每个交换机创建相同的vlan

交换机之间的链路配置为trunk

交换机启用stp协议，配置模式为mstp

交换机指定mstp的域名，实例和vlan对应该关系

交换机上激活mstp配置

## 6：验证与测试mstp配置

![](../day04/images/mstp.jpg.png)

项目需求：pc1和pc2网络互通

​                pc1属于vlan10 从交换机3口的g0/0/13口出，到达交换机1，在到路由1，在到路由2，在到交换机2在到交换机3在回来pc2

### 6-1：Route01的基本配置静态路由

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route01
[Route01]int GigabitEthernet 0/0/1
[Route01-GigabitEthernet0/0/1]ip add 192.168.10.254 24
[Route01-GigabitEthernet0/0/1]int g0/0/0
[Route01-GigabitEthernet0/0/0]ip add 192.168.12.1 24
[Route01]ip route-static 192.168.20.0 24 192.168.12.2
```

### 6-2：Route02的基本配置静态路由

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route02
[Route02]int GigabitEthernet 0/0/0
[Route02-GigabitEthernet0/0/0]ip add 192.168.12.2 24
[Route02-GigabitEthernet0/0/0]int g0/0/1
[Route02-GigabitEthernet0/0/1]ip add 192.168.20.254 24
[Route02-GigabitEthernet0/0/1]q
[Route02]ip route-static 192.168.10.0 24 192.168.12.1
```

### 6-3：交换机1配置

```java
<Huawei>undo terminal monitor
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch01	
[Switch01]vlan batch 10 20
Info: This operation may take a few seconds. Please wait for a moment...done.
[Switch01]int Eth0/0/1	
[Switch01-Ethernet0/0/1]port link-type access 
[Switch01-Ethernet0/0/1]port default vlan 10
[Switch01-Ethernet0/0/1]quit
[Switch01]port-group group-member Ethernet 0/0/12 e0/0/13
[Switch01-port-group]port link-type trunk 
[Switch01-Ethernet0/0/12]port link-type trunk 
[Switch01-Ethernet0/0/13]port link-type trunk 	
[Switch01-port-group]port trunk allow-pass vlan all
[Switch01-Ethernet0/0/12]port trunk allow-pass vlan all
[Switch01-Ethernet0/0/13]port trunk allow-pass vlan all
```

### 6-4：交换机2配置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch02
[Switch02]vlan batch 10 20
Info: This operation may take a few seconds. Please wait for a moment...done.
[Switch02]int Eth0/0/	
[Switch02-Ethernet0/0/1]port link-type access 
[Switch02-Ethernet0/0/1]port default vlan 20
[Switch02-Ethernet0/0/1]q
[Switch02]port-group group-member Ethernet 0/0/12 Ethernet 0/0/14
[Switch02-port-group]port link-type trunk 
[Switch02-Ethernet0/0/12]port link-type trunk 
[Switch02-Ethernet0/0/14]port link-type trunk 	
[Switch02-port-group]port trunk allow-pass vlan all
[Switch02-Ethernet0/0/12]port trunk allow-pass vlan all
[Switch02-Ethernet0/0/14]port trunk allow-pass vlan all
```

### 6-5：交换机3配置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch03
[Switch03]vlan batch 10 20
Info: This operation may take a few seconds. Please wait for a moment...done.
[Switch03]int Eth0/0/1	
[Switch03-Ethernet0/0/1]port link-type access 
[Switch03-Ethernet0/0/1]port default vlan 10
[Switch03-Ethernet0/0/1]int e0/0/2	
[Switch03-Ethernet0/0/2]port link-type access 
[Switch03-Ethernet0/0/2]port default vlan 20
[Switch03-Ethernet0/0/2]q	
[Switch03]port-group group-member Ethernet 0/0/13 Ethernet 0/0/14
[Switch03-port-group]port link-type trunk 
[Switch03-Ethernet0/0/13]port link-type trunk 
[Switch03-Ethernet0/0/14]port link-type trunk 	
[Switch03-port-group]port trunk allow-pass vlan all
[Switch03-Ethernet0/0/13]port trunk allow-pass vlan all
[Switch03-Ethernet0/0/14]port trunk allow-pass vlan all
```

6-6：PC1 ping PC2

```PHP
PC>ping 192.168.20.1

Ping 192.168.20.1: 32 data bytes, Press Ctrl_C to break
Request timeout!
Request timeout!
From 192.168.20.1: bytes=32 seq=3 ttl=126 time=125 ms
From 192.168.20.1: bytes=32 seq=4 ttl=126 time=125 ms
From 192.168.20.1: bytes=32 seq=5 ttl=126 time=141 ms

--- 192.168.20.1 ping statistics ---
  5 packet(s) transmitted
  3 packet(s) received
  40.00% packet loss
  round-trip min/avg/max = 0/130/141 ms
```

6-6：交换机1、2、3 ，mstp的配置

```java
<Switch01>system-view 
Enter system view, return user view with Ctrl+Z.
[Switch01]stp mode mstp                              //配置STP模式为MSTP
[Switch01]stp region-configuration                   //配置mstp域
[Switch01-mst-region] region-name ruhua              //mstp域名为ruhua
[Switch01-mst-region] instance 1 vlan 10             //第1个实例（生成树）传输的是vlan10的数据
[Switch01-mst-region] instance 2 vlan 20             //第2个实例（生成树）传输的是vlan20的数据
[Switch01-mst-region] active region-configuration    //激活MSTP域
Info: This operation may take a few seconds. Please wait for a moment...done.
[Switch01-mst-region]display this
#
stp region-configuration
 region-name ruhua
 instance 1 vlan 10
 instance 2 vlan 20
 active region-configuration
#
return
[Switch01-mst-region]
```

### 6-7：配置交换机1为vlan 10 的根交换机，vlan20 的备份交换机

```java
<Switch01>system-view 
Enter system view, return user view with Ctrl+Z.
[Switch01]stp instance 1 priority 4096                //设置第1个实现的优先级别为 4096
[Switch01]stp instance 2 priority 8192                //设置第2个实现的优先级别为 8192
```

### 6-8：配置交换机2为vlan 20 的根交换机，vlan10 的备份交换机

```java
<Route02>undo terminal monitor 
Info: Current terminal monitor is off.
<Route02>system-view 
Enter system view, return user view with Ctrl+Z.
[Route02]stp instance 2 priority 4096	             //设置第2个实现的优先级别为 4096
[Route02]stp instance 1 priority 8192                //设置第1个实现的优先级别为 8192
```

用抓包工具进行测试
