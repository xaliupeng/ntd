



# Day05

## 1:路由器

​     前面我们学习了同一个网段的的节点是如何进行数据转发，同一网段是在由交换机的MAC地址表进行数据转发。不同网段，我们就用到了路由器，他是由路由器中的路由表进行数据转发。而路由表，就分静态路由和动态路由。静态路由有分：默认路由，直连路由，手动路由；动态理由：通过OSPF协议，各路由之间学习到对方的路由条目。

###  1-1：路由器如何工作

​       根据路由表选择最佳路径，每个路由表都维护着一张路由表，这是转发数据的关键，每条路由表记录指明了，到达某个子网或主机应从路由器的那个物理端口发送，通过此端口可到达该路径的下一个路由器的地址（或直接相连网络中的目标主机地址）

查看路由表

```java
<Huawei>display ip routing-table                   //查看路由表
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Tables: Public
         Destinations : 4        Routes : 4        

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

      127.0.0.0/8   Direct  0    0           D   127.0.0.1       InLoopBack0
      127.0.0.1/32  Direct  0    0           D   127.0.0.1       InLoopBack0
127.255.255.255/32  Direct  0    0           D   127.0.0.1       InLoopBack0
255.255.255.255/32  Direct  0    0           D   127.0.0.1       InLoopBack0
```

路由表中包含了一条条路由条目，而路由条目有很多类型

依据来源的不同，路由可以分为三类：

​    通过链路层协议发现在的路由称为直连路由

​    通过网络管理员手动配置的路由称为静态路由

  通过动态路由协议发现地路由称为动态路由

 静态路由

​    由管理 员手工配置，为单向条目，通信双方的路由器都需要配置路由，否则会导致数据包有去无回。

   默认路由是一种特殊的静态路由，默认路由的目标网络为0.0.0.0/0.0.0.0.匹配任何目标地址，只有当从路由表中找不到任何明确匹配的路由条目时，才会使用默认路由。（最长匹配原则）

### 1-2：静态路由的配置命令

​    使用 ip route-static 命令

​    指定到达的目的网络

​    基本格式

```java
[Huawei]ip route-static 目标网络 子网掩码 下一跳
```



![](../day05/images/route.jpg)

pc1的网络设置

![](../day05/images/gateway.jpg)



Route1的设备配置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route1
[Route1]int GigabitEthernet 0/0/0
[Route1-GigabitEthernet0/0/0]ip add 192.168.100.1 24    //路由器接口 0 设置 IP 地址
[Route1-GigabitEthernet0/0/0]int g0/0/1
[Route1-GigabitEthernet0/0/1]ip add 192.168.1.254 24    //路由器接口 1 设置 IP 地址
[Route1-GigabitEthernet0/0/1]q
[Route1]ip route-static 192.168.2.0 24 192.168.100.2    //数据包进入192.168.2.0网段的下一跳地址是192.168.100.2
```

Route3的设备配置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route3
[Route3]int GigabitEthernet0/0/1
[Route3-GigabitEthernet0/0/1]ip add 192.168.2.254 24     //路由器接口 1 设置 IP 地址
[Route3]int GigabitEthernet0/0/0
[Route3-GigabitEthernet0/0/0]ip add 192.168.100.2 24     //路由器接口 0 设置 IP 地址
[Route3-GigabitEthernet0/0/0]q
[Route3]ip route-static 192.168.1.0 24 192.168.100.1     //数据包进入192.168.1.0网段的下一跳地址是192.168.100.1
```

pc1 ping pc2

```java
PC>ping 192.168.2.1

Ping 192.168.2.1: 32 data bytes, Press Ctrl_C to break
From 192.168.2.1: bytes=32 seq=1 ttl=126 time=125 ms
From 192.168.2.1: bytes=32 seq=2 ttl=126 time=422 ms
From 192.168.2.1: bytes=32 seq=3 ttl=126 time=109 ms
From 192.168.2.1: bytes=32 seq=4 ttl=126 time=110 ms
From 192.168.2.1: bytes=32 seq=5 ttl=126 time=93 ms

--- 192.168.2.1 ping statistics ---
  5 packet(s) transmitted
  5 packet(s) received
  0.00% packet loss
  round-trip min/avg/max = 93/171/422 ms
```

静态路由二：

![](../day05/images/StaticRoute.jpg.png)



Route1 基本设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route1
[Route1]int GigabitEthernet 0/0/0
[Route1-GigabitEthernet0/0/0]ip add 192.168.100.1 24	
[Route1-GigabitEthernet0/0/0]int g0/0/1
[Route1-GigabitEthernet0/0/1]ip add 192.168.1.254 24
[Route1-GigabitEthernet0/0/1]q	
[Route1]ip route-static 192.168.2.0 24 192.168.100.2
```

Route2 基本设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route2
[Route2]int GigabitEthernet0/0/0
[Route2-GigabitEthernet0/0/0]ip add 192.168.100.2 24
[Route2-GigabitEthernet0/0/0]int g0/0/2
[Route2-GigabitEthernet0/0/2]ip add 192.168.200.1 24
[Route2-GigabitEthernet0/0/2]q
[Route2]ip route-static 192.168.2.0 24 192.168.200.2  //去192.168.2.0的路由
[Route2]ip route-static 192.168.1.0 24 192.168.100.1  //回192.168.1.0的路由
```

Route3 基本设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route3
[Route3]int GigabitEthernet0/0/2
[Route3-GigabitEthernet0/0/2]ip add 192.168.200.2 24	
[Route3-GigabitEthernet0/0/2]int g0/0/1
[Route3-GigabitEthernet0/0/1]ip add 192.168.2.254 24
[Route3-GigabitEthernet0/0/1]q
[Route3]ip route-static 192.168.1.0 24 192.168.200.1
```

pc1 ping pc2

```java
PC>ping 192.168.2.1

Ping 192.168.2.1: 32 data bytes, Press Ctrl_C to break
Request timeout!
From 192.168.2.1: bytes=32 seq=2 ttl=125 time=188 ms
From 192.168.2.1: bytes=32 seq=3 ttl=125 time=141 ms
From 192.168.2.1: bytes=32 seq=4 ttl=125 time=125 ms
From 192.168.2.1: bytes=32 seq=5 ttl=125 time=93 ms

--- 192.168.2.1 ping statistics ---
  5 packet(s) transmitted
  4 packet(s) received
  20.00% packet loss
  round-trip min/avg/max = 0/136/188 ms
```

pc1 tracert pc2

```java
PC>tracert 192.168.2.1

traceroute to 192.168.2.1, 8 hops max
(ICMP), press Ctrl+C to stop
 1  192.168.1.254   15 ms  47 ms  47 ms
 2  192.168.100.2   63 ms  46 ms  79 ms
 3  192.168.200.2   62 ms  63 ms  93 ms
 4  192.168.2.1   110 ms  109 ms  125 ms

PC>
```

查询ip信息：

```java
<Route1>display ip interface brief 
*down: administratively down
^down: standby
(l): loopback
(s): spoofing
The number of interface that is UP in Physical is 3
The number of interface that is DOWN in Physical is 1
The number of interface that is UP in Protocol is 3
The number of interface that is DOWN in Protocol is 1

Interface                         IP Address/Mask      Physical   Protocol  
GigabitEthernet0/0/0              192.168.100.1/24     up         up        
GigabitEthernet0/0/1              192.168.1.254/24     up         up        
GigabitEthernet0/0/2              unassigned           down       down      
NULL0                             unassigned           up         up(s)   
```

查询路由表：

```java
<Route1>display ip routing-table 
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Tables: Public
         Destinations : 11       Routes : 11       

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

      127.0.0.0/8   Direct  0    0           D   127.0.0.1       InLoopBack0
      127.0.0.1/32  Direct  0    0           D   127.0.0.1       InLoopBack0
127.255.255.255/32  Direct  0    0           D   127.0.0.1       InLoopBack0
    192.168.1.0/24  Direct  0    0           D   192.168.1.254   GigabitEthernet0/0/1
  192.168.1.254/32  Direct  0    0           D   127.0.0.1       GigabitEthernet0/0/1
  192.168.1.255/32  Direct  0    0           D   127.0.0.1       GigabitEthernet0/0/1
    192.168.2.0/24  Static  60   0          RD   192.168.100.2   GigabitEthernet0/0/0
  192.168.100.0/24  Direct  0    0           D   192.168.100.1   GigabitEthernet0/0/0
  192.168.100.1/32  Direct  0    0           D   127.0.0.1       GigabitEthernet0/0/0
192.168.100.255/32  Direct  0    0           D   127.0.0.1       GigabitEthernet0/0/0
255.255.255.255/32  Direct  0    0           D   127.0.0.1       InLoopBack0
```

查看静态路由表：

```java
<Route1>display ip routing-table protocol static 
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Public routing table : Static
         Destinations : 1        Routes : 1        Configured Routes : 1

Static routing table status : <Active>
         Destinations : 1        Routes : 1

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

    192.168.2.0/24  Static  60   0          RD   192.168.100.2   GigabitEthernet0/0/0

Static routing table status : <Inactive>
         Destinations : 0        Routes : 0

<Route1>
```

查看去住一个IP目地的的IP路由

```php
<Route1>display ip routing-table 192.168.2.0
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Table : Public
Summary Count : 1
Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

192.168.2.0/24  Static  60   0          RD   192.168.100.2   GigabitEthernet0/0/0
```

1-3: 默认路由注意事项

​     两个相对的路由器在书写默认路由时，不能同时使用默认路由，这样会引起路由环路。路由环路虽然没有arp广播那样厉害，但是也会对网络产生一定的影响，而数据包每次经过一个路由器，他的TTL就会减少一次，直至TTL为0.

Route1:

```
<Route1>sys	
<Route1>system-view 
Enter system view, return user view with Ctrl+Z.
[Route1]ip rout	
[Route1]ip route-s	
[Route1]ip route-static 0.0.0.0 0.0.0.0 192.168.100.2
[Route1]
```

Route2:

```
<Route2>system-view 
Enter system view, return user view with Ctrl+Z.
[Route2]ip rout	
[Route2]ip route-st	
[Route2]ip route-static 0.0.0.0 0 192.168.100.1
[Route2]
```

tracert 测试结果

```java
[Route1]tracert 8.8.8.8

 traceroute to  8.8.8.8(8.8.8.8), max hops: 30 ,packet length: 40,press CTRL_C t
o break 

 1 192.168.100.2 60 ms  30 ms  30 ms 

 2 192.168.100.1 40 ms  10 ms  90 ms 

 3 192.168.100.2 40 ms  10 ms  40 ms 

 4 192.168.100.1 40 ms  50 ms  40 ms 

 5 192.168.100.2 70 ms  60 ms  60 ms 

 6 192.168.100.1 70 ms  50 ms  70 ms 

 7 192.168.100.2 80 ms  90 ms  80 ms 

 8 192.168.100.1 80 ms  100 ms  60 ms 

 9 192.168.100.2 90 ms  110 ms  90 ms 

10 192.168.100.1 110 ms  110 ms  110 ms
```

总结：

路由：

​      不同网段之间的设备通信，称之为路由

​       具备路由功能的设备（有路由表），常见的有：路由器/多层交换机

​      当路由设备接收到一个数据包以后，直接查看目标IP地址和自己的路由表：

​       如果有匹配的路由条目，则从对应的端口转发出去

​       如果没有匹配的路由条目，直接丢弃

​       如果，在同一个路由表中，有多个路由条目同是匹配了一个目标IP，此时，路由器会按照“最长匹配原则”，选择一个“最精准”的路由条目

比如：

R1 :收到一个数据包，目标地址是192.168.100.1

@1. 192.168.100.0     255.255.255.0    --------> A

@2.  0.0.0.0                 0.0.0.0             ---------->B

@3.192.168.0.0           255.255.0.0      ---------->C



命令：

​        查看路由表的命令：

​      display ip routing-table   //查看整个大的路由表；

​      display ip

查看去往特定目标IP地址的路由条目的命令;

​      display ip routing-table x.x.x.x

--类型：

​        直连路由

​        静态路由

​                  &特殊的路由条目（默认路由 0.0.0.0/0）

​                  即一个路由条目，表示了“所有目标ip”的路由条目；通常默认路由，都是配置在公司的边界设备上；同时，直接相连的两个设备不能互相配置默认路由   

​         动态路由（后面学习）

### 1-3：单臂路由      

![](../day05/images/dbroute.png)

路由器设置：

```java

<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route
[Route]int GigabitEthernet0/0/1.10    //进入子接口
[Route-GigabitEthernet0/0/1.10]
[Route-GigabitEthernet0/0/1.10]dot1q termination vid 10  //指定端口属于vlan 10  
[Route-GigabitEthernet0/0/1.10]ip add 10.10.0.1 24	     //指定端口的IP地址
[Route-GigabitEthernet0/0/1.10]arp broadcast enable      //开启子接口的ARP广播
[Route-GigabitEthernet0/0/1.10]q
[Route]int GigabitEthernet0/0/1.20	
[Route-GigabitEthernet0/0/1.20]dot1q termination vid 20
[Route-GigabitEthernet0/0/1.20]ip add 10.20.0.1 24
[Route-GigabitEthernet0/0/1.20]arp broadcast enable 
[Route-GigabitEthernet0/0/1.20]q
```

交换机设置：

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch
[Switch]int Eth0/0/1
[Switch-Ethernet0/0/1]port link-type access 
[Switch-Ethernet0/0/1]q
[Switch]vlan batch 10 20
Info: This operation may take a few seconds. Please wait for a moment...done.
[Switch]int Eth0/0/1	
[Switch-Ethernet0/0/1]port default vlan 10
[Switch-Ethernet0/0/1]int e0/0/2
[Switch-Ethernet0/0/2]port link-type access 	
[Switch-Ethernet0/0/2]port default vlan 20
[Switch-Ethernet0/0/2]int e0/0/3
[Switch-Ethernet0/0/3]port link-type trunk 
[Switch-Ethernet0/0/3]port trunk allow-pass vlan all

```

pc1 ping pc2

```java
PC>ping 10.20.0.20

Ping 10.20.0.20: 32 data bytes, Press Ctrl_C to break
From 10.20.0.20: bytes=32 seq=1 ttl=127 time=109 ms
From 10.20.0.20: bytes=32 seq=2 ttl=127 time=110 ms
From 10.20.0.20: bytes=32 seq=3 ttl=127 time=78 ms
From 10.20.0.20: bytes=32 seq=4 ttl=127 time=78 ms
From 10.20.0.20: bytes=32 seq=5 ttl=127 time=78 ms

--- 10.20.0.20 ping statistics ---
  5 packet(s) transmitted
  5 packet(s) received
  0.00% packet loss
  round-trip min/avg/max = 78/90/110 ms

PC>
```

### 1-4：vlan之间配置网关相互通信

![](../day05/images/SwitchRoute.png)

交换机设置：

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]vlan batch 1 2 3
Info: This operation may take a few seconds. Please wait for a moment...done.
[Huawei]int Vlanif 1
[Huawei-Vlanif1]ip add 192.168.1.254 24
[Huawei-Vlanif1]q
[Huawei]int Vlanif 2
[Huawei-Vlanif2]ip add 192.168.2.254 24
[Huawei-Vlanif2]q	
[Huawei]int Vlanif 3
[Huawei-Vlanif3]ip add 192.168.3.254 24
[Huawei-Vlanif3]q	
[Huawei]sysname Switch
[Switch]int GigabitEthernet 0/0/1
[Switch-GigabitEthernet0/0/1]port link-type access 	
[Switch-GigabitEthernet0/0/1]port default vlan 1
[Switch-GigabitEthernet0/0/1]int g0/0/2
[Switch-GigabitEthernet0/0/2]port link-type access 	
[Switch-GigabitEthernet0/0/2]port default vlan 2
[Switch-GigabitEthernet0/0/2]int g0/0/3
[Switch-GigabitEthernet0/0/3]port link-type access 	
[Switch-GigabitEthernet0/0/3]port default vlan 3
```

查看交换机上的直连路由

```java
[Switch]display ip routing-table 
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Tables: Public
         Destinations : 8        Routes : 8        

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

      127.0.0.0/8   Direct  0    0           D   127.0.0.1       InLoopBack0
      127.0.0.1/32  Direct  0    0           D   127.0.0.1       InLoopBack0
    192.168.1.0/24  Direct  0    0           D   192.168.1.254   Vlanif1
  192.168.1.254/32  Direct  0    0           D   127.0.0.1       Vlanif1
    192.168.2.0/24  Direct  0    0           D   192.168.2.254   Vlanif2
  192.168.2.254/32  Direct  0    0           D   127.0.0.1       Vlanif2
    192.168.3.0/24  Direct  0    0           D   192.168.3.254   Vlanif3
  192.168.3.254/32  Direct  0    0           D   127.0.0.1       Vlanif3
```

### 1-5：学生练习

![](../day05/images/vlanRoute.png)



### 1-6：vlan 之间全网互通

![](../day05/images/fullVlan.png)

#### 1-6-1:Switch1/2/3/4/5

```java
undo terminal monitor
system-view
vlan batch 10 20 30 40 12 23
sysname Switch
```

#### 1-6-2:Swithc1

```java
[Switch1]int GigabitEthernet 0/0/1
[Switch1-GigabitEthernet0/0/1]port link-type access 
[Switch1-GigabitEthernet0/0/1]port default vlan 10
[Switch1-GigabitEthernet0/0/1]q
[Switch1]int Vlanif 10
[Switch1-Vlanif10]ip add 192.168.10.254 24
[Switch1-Vlanif10]q
[Switch1]int Vlanif 12
[Switch1-Vlanif12]ip add 192.168.12.1 24
[Switch1-Vlanif12]q	
[Switch1]port-group group-member GigabitEthernet 0/0/12 GigabitEthernet 0/0/20	
[Switch1-port-group]port link-type trunk 
[Switch1-GigabitEthernet0/0/12]port link-type trunk 
[Switch1-GigabitEthernet0/0/20]port link-type trunk 
[Switch1-port-group]port trunk allow-pass vlan all
[Switch1-GigabitEthernet0/0/12]port trunk allow-pass vlan all
[Switch1-GigabitEthernet0/0/20]port trunk allow-pass vlan all
[Switch1-port-group]
```

#### 1-6-3:Switch2

```java
[Switch2]int Vlanif 20
[Switch2-Vlanif20]ip add 192.168.20.254 24
[Switch2-Vlanif20]q
[Switch2]int Vlanif 12
[Switch2-Vlanif12]ip add 192.168.12.2 24
[Switch2-Vlanif12]q
[Switch2]int Vlanif 23
[Switch2-Vlanif23]ip add 192.168.23.1 24
[Switch2-Vlanif23]q	
[Switch2]port-group group-member GigabitEthernet 0/0/12 GigabitEthernet 0/0/21 to GigabitEthernet 0/0/23
[Switch2-port-group]port link-type trunk 
[Switch2-GigabitEthernet0/0/12]port link-type trunk 
[Switch2-GigabitEthernet0/0/21]port link-type trunk 
[Switch2-GigabitEthernet0/0/22]port link-type trunk 
[Switch2-GigabitEthernet0/0/23]port link-type trunk 	
[Switch2-port-group]port trunk allow-pass vlan all
[Switch2-GigabitEthernet0/0/12]port trunk allow-pass vlan all
[Switch2-GigabitEthernet0/0/21]port trunk allow-pass vlan all
[Switch2-GigabitEthernet0/0/22]port trunk allow-pass vlan all
[Switch2-GigabitEthernet0/0/23]port trunk allow-pass vlan all
[Switch2-port-group]q
[Switch2]
```

#### 1-6-4:Switch3

```
[Switch3]int Vlanif 23
[Switch3-Vlanif23]ip add 192.168.23.2 24
[Switch3-Vlanif23]q
[Switch3]int Vlanif 30
[Switch3-Vlanif30]ip add 192.168.30.254 24
[Switch3-Vlanif30]q	
[Switch3]int Vlanif 40
[Switch3-Vlanif40]ip add 192.168.40.254 24
[Switch3-Vlanif40]q
[Switch3]int GigabitEthernet 0/0/1
[Switch3-GigabitEthernet0/0/1]port link-type access 	
[Switch3-GigabitEthernet0/0/1]port default vlan 40
[Switch3-GigabitEthernet0/0/1]
```

#### 1-6-5:Switch4

```java
[Switch4]port-group group-member Ethernet 0/0/20 GigabitEthernet 0/0/2 Ethernet 0/0/22
[Switch4-port-group]port link-type trunk 
[Switch4-Ethernet0/0/20]port link-type trunk 
[Switch4-GigabitEthernet0/0/2]port link-type trunk 
[Switch4-Ethernet0/0/22]port link-type trunk 
[Switch4-port-group]port trunk allow-pass vlan all
[Switch4-Ethernet0/0/20]port trunk allow-pass vlan all
[Switch4-GigabitEthernet0/0/2]port trunk allow-pass vlan all
[Switch4-Ethernet0/0/22]port trunk allow-pass vlan all
[Switch4-port-group]q
[Switch4]int Eth0/0/1	
[Switch4-Ethernet0/0/1]port link-type access 
[Switch4-Ethernet0/0/1]port default vlan 20	
[Switch4-Ethernet0/0/1]int e0/0/2	
[Switch4-Ethernet0/0/2]port link-type access 
[Switch4-Ethernet0/0/2]port default vlan 10
[Switch4-Ethernet0/0/2]q
[Switch4]
```

####  1-6-6:Switch5

```java
[Switch5]port-group group-member Ethernet 0/0/21 Ethernet 0/0/22
[Switch5-port-group]port link-type trunk 
[Switch5-Ethernet0/0/21]port link-type trunk 
[Switch5-Ethernet0/0/22]port link-type trunk 	
[Switch5-port-group]port trunk allow-pass vlan all
[Switch5-Ethernet0/0/21]port trunk allow-pass vlan all
[Switch5-Ethernet0/0/22]port trunk allow-pass vlan all
[Switch5-port-group]q	
[Switch5]int Eth0/0/1	
[Switch5-Ethernet0/0/1]port link-type access 
[Switch5-Ethernet0/0/1]port default vlan 30
[Switch5-Ethernet0/0/1]
```

#### 1-6-7:交换机1上的路由表

```java
[Switch1]display ip routing-table 
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Tables: Public
         Destinations : 9        Routes : 9        

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

      127.0.0.0/8   Direct  0    0           D   127.0.0.1       InLoopBack0
      127.0.0.1/32  Direct  0    0           D   127.0.0.1       InLoopBack0
   192.168.10.0/24  Direct  0    0           D   192.168.10.254  Vlanif10
 192.168.10.254/32  Direct  0    0           D   127.0.0.1       Vlanif10
   192.168.12.0/24  Direct  0    0           D   192.168.12.1    Vlanif12
   192.168.12.1/32  Direct  0    0           D   127.0.0.1       Vlanif12
   192.168.20.0/24  Static  60   0          RD   192.168.12.2    Vlanif12
   192.168.30.0/24  Static  60   0          RD   192.168.12.2    Vlanif12
   192.168.40.0/24  Static  60   0          RD   192.168.12.2    Vlanif12
```

#### 1-6-8:交换机2上的路由表

```java
[Switch2]display ip routing-table 
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Tables: Public
         Destinations : 11       Routes : 11       

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

      127.0.0.0/8   Direct  0    0           D   127.0.0.1       InLoopBack0
      127.0.0.1/32  Direct  0    0           D   127.0.0.1       InLoopBack0
   192.168.10.0/24  Static  60   0          RD   192.168.12.1    Vlanif12
   192.168.12.0/24  Direct  0    0           D   192.168.12.2    Vlanif12
   192.168.12.2/32  Direct  0    0           D   127.0.0.1       Vlanif12
   192.168.20.0/24  Direct  0    0           D   192.168.20.254  Vlanif20
 192.168.20.254/32  Direct  0    0           D   127.0.0.1       Vlanif20
   192.168.23.0/24  Direct  0    0           D   192.168.23.1    Vlanif23
   192.168.23.1/32  Direct  0    0           D   127.0.0.1       Vlanif23
   192.168.30.0/24  Static  60   0          RD   192.168.23.2    Vlanif23
   192.168.40.0/24  Static  60   0          RD   192.168.23.2    Vlanif23
```

#### 1-6-9:交换机3上的路由表

```java
[Switch3]display ip routing-table 
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Tables: Public
         Destinations : 10       Routes : 10       

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

        0.0.0.0/0   Static  60   0          RD   192.168.23.1    Vlanif23
      127.0.0.0/8   Direct  0    0           D   127.0.0.1       InLoopBack0
      127.0.0.1/32  Direct  0    0           D   127.0.0.1       InLoopBack0
   192.168.10.0/24  Static  60   0          RD   192.168.23.1    Vlanif23
   192.168.23.0/24  Direct  0    0           D   192.168.23.2    Vlanif23
   192.168.23.2/32  Direct  0    0           D   127.0.0.1       Vlanif23
   192.168.30.0/24  Direct  0    0           D   192.168.30.254  Vlanif30
 192.168.30.254/32  Direct  0    0           D   127.0.0.1       Vlanif30
   192.168.40.0/24  Direct  0    0           D   192.168.40.254  Vlanif40
 192.168.40.254/32  Direct  0    0           D   127.0.0.1       Vlanif40
```

#### 1-6-10:交换机4上的路由表

```java
<Switch4>display ip routing-table 
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Tables: Public
         Destinations : 2        Routes : 2        

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

      127.0.0.0/8   Direct  0    0           D   127.0.0.1       InLoopBack0
      127.0.0.1/32  Direct  0    0           D   127.0.0.1       InLoopBack0

<Switch4>
```

#### 1-6-11:交换机5上的路由表

```java
<Switch5>display ip routing-table 
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Tables: Public
         Destinations : 2        Routes : 2        

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

      127.0.0.0/8   Direct  0    0           D   127.0.0.1       InLoopBack0
      127.0.0.1/32  Direct  0    0           D   127.0.0.1       InLoopBack0
```

1-7:如何确保最优路径：

​              同一个VLAN 的根交换机，和网关，放在同一个设备上，如此一来，每个PC访问主机的网关，都是路径最优的。
