

# Day06

## 1:浮动路由

​    到此为止：

​    我们学习了交换技术：vlan/trunk/access/hybrid/stp/mstp

​    可以确保一个网段内部互通：

​    我们学习了路由技术：直连路由/静态路由/默认路由/路由的原路

​    可以确保不同网段之间的互通：

![](../day06/images/fdRoute.jpg.png)



### 1-1：交换机 1 设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch1
[Switch1]vlan batch 10 40
Info: This operation may take a few seconds. Please wait for a moment...done.
[Switch1]int Eth0/0/1	
[Switch1-Ethernet0/0/1]port link-type access 	
[Switch1-Ethernet0/0/1]port default vlan 10
[Switch1-Ethernet0/0/1]int e0/0/2	
[Switch1-Ethernet0/0/2]port link-type access
[Switch1-Ethernet0/0/2]port default vlan 10
```

### 1-2：交换机 2 设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch2
[Switch2]vlan batch 10 40
Info: This operation may take a few seconds. Please wait for a moment...done.	
[Switch2]int Eth0/0/2	
[Switch2-Ethernet0/0/2]port link-type access 
[Switch2-Ethernet0/0/2]port default vlan 40	
[Switch2-Ethernet0/0/2]int e0/0/1	
[Switch2-Ethernet0/0/1]port link-type access	
[Switch2-Ethernet0/0/1]port default vlan 40
```

### 1-3：路由器 1 设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route1
[Route1]int g	
[Route1]int GigabitEthernet 0/0/1
[Route1-GigabitEthernet0/0/1]ip add 192.168.1.254 24
[Route1-GigabitEthernet0/0/1]int g0/0/0
[Route1-GigabitEthernet0/0/0]ip add 192.168.3.1 24
[Route1-GigabitEthernet0/0/1]q
[Route1]ip route-static 192.168.4.0 24 192.168.3.2
```

### 1-4：路由器 2 设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.	
[Huawei]sysname Route2
[Route2]int GigabitEthernet 0/0/0
[Route2-GigabitEthernet0/0/0]ip add 192.168.3.2 24
[Route2-GigabitEthernet0/0/0]int g0/0/1
[Route2-GigabitEthernet0/0/1]ip add 192.168.4.254 24
[Route2-GigabitEthernet0/0/1]q
[Route2]ip route-static 192.168.1.0 24 192.168.3.1
```

### 1-5：PC 1 ping PC2

```JAVA
PC>ping 192.168.4.1

Ping 192.168.4.1: 32 data bytes, Press Ctrl_C to break
From 192.168.4.1: bytes=32 seq=1 ttl=126 time=62 ms
From 192.168.4.1: bytes=32 seq=2 ttl=126 time=78 ms
From 192.168.4.1: bytes=32 seq=3 ttl=126 time=78 ms
From 192.168.4.1: bytes=32 seq=4 ttl=126 time=78 ms
From 192.168.4.1: bytes=32 seq=5 ttl=126 time=79 ms

--- 192.168.4.1 ping statistics ---
  5 packet(s) transmitted
  5 packet(s) received
  0.00% packet loss
  round-trip min/avg/max = 62/75/79 ms
```

为了线路更加安全，靠谱，我们给他有增加了一条冗余链路，具体设置如下：

设置如下：

```java
<Route1>undo terminal monitor 
Info: Current terminal monitor is off.
<Route1>system-view 
Enter system view, return user view with Ctrl+Z.
[Route1]int GigabitEthernet 0/0/2	
[Route1-GigabitEthernet0/0/2]ip address 192.168.2.1 24
[Route1-GigabitEthernet0/0/2]q
[Route1]ip route-static 192.168.4.0 24 192.168.2.2 preference 61 //去往192.168.4.0网段的路由比上一条优先级大，路由优先级越小越好
[Route1]display ip routing-table 192.168.4.0
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Table : Public
Summary Count : 1
Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

    192.168.4.0/24  Static  60   0          RD   192.168.3.2     GigabitEthernet
0/0/0
[Route1]interface GigabitEthernet 0/0/0
[Route1-GigabitEthernet0/0/0]shutdown   //人为的创造一个故障关闭0接口
[Route1-GigabitEthernet0/0/0]q
[Route1]display ip routing-table 192.168.4.0  //查看去往4.0网段的路由
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Table : Public
Summary Count : 1
Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

    192.168.4.0/24  Static  61   0          RD   192.168.2.2     GigabitEthernet   //静态路由的COS值不变等于 0 所认只有修改静态路由的优先级
0/0/2
```

查看路由表

```java
<Route1>display ip routing-table protocol static 
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Public routing table : Static
         Destinations : 1        Routes : 2        Configured Routes : 2

Static routing table status : <Active>       //活动
         Destinations : 1        Routes : 1

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

    192.168.4.0/24  Static  61   0          RD   192.168.2.2     GigabitEthernet
0/0/2

Static routing table status : <Inactive>    //非活动
         Destinations : 1        Routes : 1

Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

    192.168.4.0/24  Static  60   0               192.168.3.2     Unknown

```

### 1-6：总结    

 在“路由网络”中，依然存在“单点故障”。在路由网络中的单点故障的类型，包括：

​         ---线路故障

​         ---设备故障

解决方案：

​        --增加冗余的线路

​       --增加冗余的设备

新的问题：

​       如果在设备之间添加了冗余的线路，那么正常情况下两个线路都可以使用的时候，我们应该确保：一个主链路，一个备份链路。即主链路对应的路由条目，应该加入到路由表中;备份链路对应的路由条目，就不能进入到路由表中; 

实现方法：

​        修改路由条目的属性，以确保在去往同一个网段的时候，如果存在多个下一跳设备，我们通过比较这些“路由属性”来区分路由的好坏;

​       ---优先级（Preference), 表示的是路由条目的稳定性；越小越好；

​      ---开销值  （cos）,表示的是该路由器去住网段的距离；越小越好；

对于任何一种类型的路由条目，优先级都存在一个默认值。静态路由的“默认路由优先级”是60想要修改静态路由条目的优先级。命令如下：

ip route-static 网段 掩码 下一跳 preference **

//** 取值范围是1---255

注意：上述这种“修改路由优先级”实现链路备份的方法，称为之浮动路由/备份路由

## 2：VRRP 虚拟网关

​         VRRP 

​         将多台路由器虚拟成一个虚拟路由器，配置一个虚拟的网关IP地址

​         虚拟网关IP地址配置在主机上，实现网关备份

<img src="../day06/images/VRRP1.png" style="zoom:80%;" />

### 2-1：VRRP协议 

Virtual Router Redundancy Protocol ,虚拟路由器冗余协议

由IETF标准RFC 2338定义，是公有标准协议

VRRP协议 位于OSI模型第三层，协议号为112 （tcp 6,udp 17.icmp 1 ）

VRRP发送报文的方式为组播，地址为224.0.0.18

### 2-1：VRRP组成员角色

主（Master）路由器

备（Backup）路由器

虚拟（Virtual）路由器

### 2-3：VRRP主/备角色选举原则 （默认级别为 100）

首先比较优先级，越大越好

其次比较IP地址，越大越好

![](../day06/images/VRRP.png)

路由器 1 设置：

```java
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route1
[Route1]int GigabitEthernet 0/0/0
[Route1-GigabitEthernet0/0/0]ip add 192.168.1.250 24
[Route1-GigabitEthernet0/0/0]vrrp vrid 1 virtual-ip 192.168.1.254 
[Route1-GigabitEthernet0/0/0]vrrp vrid 1 priority 200
[Route1-GigabitEthernet0/0/0]q
[Route1]display vrrp brief          //验证 vrrp
Total:1     Master:1     Backup:0     Non-active:0      
VRID  State        Interface                Type     Virtual IP     
----------------------------------------------------------------
1     Master       GE0/0/0                  Normal   192.168.1.254  
```

路由器 2 设置：

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route2	
[Route2-GigabitEthernet0/0/1]int g0/0/0
[Route2-GigabitEthernet0/0/0]ip add 192.168.1.251 24
[Route2-GigabitEthernet0/0/0]vrrp vrid 1 virtual-ip 192.168.1.254
[Route2-GigabitEthernet0/0/0]q
[Route2]display vrrp brief 
Total:1     Master:0     Backup:1     Non-active:0      
VRID  State        Interface                Type     Virtual IP     
----------------------------------------------------------------
1     Backup       GE0/0/0                  Normal   192.168.1.254  
[Route2]
```

## 3：VRRP (2)

![](../day06/images/mastervrrp.png)

### 3-1 ：交换机 Lsw1 设置

```java
The device is running!

<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Switch1
[Switch1]vlan 10
[Switch1-vlan10]q
[Switch1]int GigabitEthernet 0/0/10
[Switch1-GigabitEthernet0/0/10]port link-type access 
[Switch1-GigabitEthernet0/0/10]port default vlan 10
[Switch1-GigabitEthernet0/0/10]q
[Switch1]port-group group-member GigabitEthernet 0/0/1 GigabitEthernet 0/0/2	
[Switch1-port-group]port link-type access 
[Switch1-GigabitEthernet0/0/1]port link-type access 
[Switch1-GigabitEthernet0/0/2]port link-type access 
[Switch1-port-group]port default vlan 10
[Switch1-GigabitEthernet0/0/1]port default vlan 10
[Switch1-GigabitEthernet0/0/2]port default vlan 10
[Switch1-port-group]q
[Switch1]
```

### 3-2：路由器 2 的设置

```java
<Route2>undo terminal monitor 
Info: Current terminal monitor is off.
<Route2>system-view 
Enter system view, return user view with Ctrl+Z.
[Route2]int GigabitEthernet 0/0/1
[Route2-GigabitEthernet0/0/1]ip add 192.168.1.251 24
[Route2-GigabitEthernet0/0/1]int g0/0/2
[Route2-GigabitEthernet0/0/2]ip add 192.168.13.1 24
[Route2-GigabitEthernet0/0/2]q
[Route2]int GigabitEthernet 0/0/1	
[Route2-GigabitEthernet0/0/1]vrrp vrid 10 virtual-ip 192.168.1.254	
[Route2-GigabitEthernet0/0/1]vrrp vrid 10 priority 200
```

### 3-3：路由器 3 的设置

```java
<Huawei>undo terminal monitor 
Info: Current terminal monitor is off.
<Huawei>system-view 
Enter system view, return user view with Ctrl+Z.
[Huawei]sysname Route3
[Route3]int GigabitEthernet 0/0/0
[Route3-GigabitEthernet0/0/0]ip add 192.168.23.1 24
[Route3-GigabitEthernet0/0/0]int g0/0/2
[Route3-GigabitEthernet0/0/2]ip add 192.168.1.252 24	
[Route3-GigabitEthernet0/0/2]vrrp vrid 10 virtual-ip 192.168.1.254
[Route3]display vrrp brief 
Total:1     Master:0     Backup:1     Non-active:0      
VRID  State        Interface                Type     Virtual IP     
----------------------------------------------------------------
10    Backup       GE0/0/2                  Normal   192.168.1.254  
[Route3]
```

### 3-4: 路由器 2 的  vrrp 信息

```java
<Route2>display vrrp brief 
Total:1     Master:1     Backup:0     Non-active:0      
VRID  State        Interface                Type     Virtual IP     
----------------------------------------------------------------
10    Master       GE0/0/1                  Normal   192.168.1.254  
<Route2>
```

### 3-5: vrrp 的链路跟踪

```java
Enter system view, return user view with Ctrl+Z.
[Route2]int GigabitEthernet 0/0/1
[Route2-GigabitEthernet0/0/1]display this
[V200R003C00]
#
interface GigabitEthernet0/0/1
 ip address 192.168.1.251 255.255.255.0 
 vrrp vrid 10 virtual-ip 192.168.1.254
 vrrp vrid 10 priority 200
#
return
[Route2-GigabitEthernet0/0/1]vrrp vrid 10 track interface GigabitEthernet 0/0/2 reduced 110
[Route2-GigabitEthernet0/0/1]q
[Route2]display vrrp brief 
Total:1     Master:1     Backup:0     Non-active:0      
VRID  State        Interface                Type     Virtual IP     
----------------------------------------------------------------
10    Master       GE0/0/1                  Normal   192.168.1.254  
[Route2]int GigabitEthernet 0/0/2	
[Route2-GigabitEthernet0/0/2]shutdown
[Route2-GigabitEthernet0/0/2]q
[Route2]display vrrp brief 
Total:1     Master:0     Backup:1     Non-active:0      
VRID  State        Interface                Type     Virtual IP     
----------------------------------------------------------------
10    Backup       GE0/0/1                  Normal   192.168.1.254  
```

故障：主网关连接外网的链路出现故障，可使用VRRP链路跟踪。当路由器2的gi0/0/2口断开后，该端口发送的VRRP优先级从200降低110.变成99，从而导致对端变成了主网关。

### 3-6 在给路由器 1 

​      加上一条去往 192.168.1.0 的路由

```java
[Route1]ip route-static 192.168.1.0 24 192.168.23.1 preference 61
[Route1]display ip routing-table 192.168.1.1
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Table : Public
Summary Count : 1
Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

    192.168.1.0/24  Static  60   0          RD   192.168.13.1    GigabitEthernet0/0/2
```

测试：

​      将路由器 2 的GI0/0/2口关掉。测试pc1----->pc2 的数据流向

​     ping   tracert   

外网故障，我们用到了网关备份（vrrp）/链路跟踪/浮动路由。

内网故障：

### 3-7：BFD （Bidirectional  Forwarding Detection）

​        双向转发检测机制，提供了一个通用的标准化的，与介质和协议无关的快速故障检测机制，用于快速检测，监控网络中链路或者IP路由的转发连通状况。

路由器 1 的设置：

```shell
[Route1]bfd     //开启BFD功能
[Route1-bfd]q
[Route1]bfd R1-->R2 bind peer-ip 192.168.1.251 source-ip 192.168.13.2
[Route1-bfd-session-R1-->R2]discriminator local 2	
[Route1-bfd-session-R1-->R2]discriminator remote 1
[Route1-bfd-session-R1-->R2]commit 
[Route1-bfd-session-R1-->R2]q
[Route1]display bfd session all
--------------------------------------------------------------------------------
Local Remote     PeerIpAddr      State     Type        InterfaceName            
--------------------------------------------------------------------------------

2     1          192.168.1.251   Down      S_IP_PEER         -                  
--------------------------------------------------------------------------------
     Total UP/DOWN Session Number : 0/1
[Route1]
```

路由器 2 的设置：

```shell
[Route2]bfd	
[Route2-bfd]q
[Route2]bfd R2--->R1 bind peer-ip 192.168.13.2 source-ip 192.168.1.251	
[Route2-bfd-session-r2--->r1]discriminator local 1
[Route2-bfd-session-r2--->r1]discriminator remote 2	
[Route2-bfd-session-r2--->r1]commit 
[Route2-bfd-session-r2--->r1]q
[Route2]display bfd session all
--------------------------------------------------------------------------------
Local Remote     PeerIpAddr      State     Type        InterfaceName            
--------------------------------------------------------------------------------

1     2          192.168.13.2    Down      S_IP_PEER         -                  
--------------------------------------------------------------------------------
     Total UP/DOWN Session Number : 0/1
[Route2]int g	
[Route2]int GigabitEthernet 0/0/1
[Route2-GigabitEthernet0/0/1]und shutdown 
Nov 23 2021 17:49:35-08:00 Route2 %%01IFPDT/4/IF_STATE(l)[0]:Interface GigabitEt
hernet0/0/1 has turned into UP state.
[Route2]display bfd session all
--------------------------------------------------------------------------------
Local Remote     PeerIpAddr      State     Type        InterfaceName            
--------------------------------------------------------------------------------

1     2          192.168.13.2    Up        S_IP_PEER         -                  
--------------------------------------------------------------------------------
     Total UP/DOWN Session Number : 1/0
```

路由器 1 的BFD状态：

```shell
[Route1]display bfd session all
--------------------------------------------------------------------------------
Local Remote     PeerIpAddr      State     Type        InterfaceName            
--------------------------------------------------------------------------------

2     1          192.168.1.251   Down      S_IP_PEER         -                  
--------------------------------------------------------------------------------
     Total UP/DOWN Session Number : 0/1
```

给路由器 1 增加一条跟踪内网GI0/0/1的返回路由

```java
[Route1]ip route-static 192.168.1.0 24 192.168.13.1 track bfd-session R1-->R2 
Info: Succeeded in modifying route.
```

PC2 返回 PC1 的路由：

```JAVA
[Route1]display ip routing-table 192.168.1.0
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Table : Public
Summary Count : 1
Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

    192.168.1.0/24  Static  60   0          RD   192.168.13.1    GigabitEthernet0/0/2
    
    
<Route2>system-view 
Enter system view, return user view with Ctrl+Z.
[Route2]int g	
[Route2]int GigabitEthernet 0/0/1
[Route2-GigabitEthernet0/0/1]shu	
[Route2-GigabitEthernet0/0/1]shutdown 

    
[Route1]display ip routing-table 192.168.1.0
Route Flags: R - relay, D - download to fib
------------------------------------------------------------------------------
Routing Table : Public
Summary Count : 1
Destination/Mask    Proto   Pre  Cost      Flags NextHop         Interface

    192.168.1.0/24  Static  61   0          RD   192.168.23.1    GigabitEthernet0/0/1

```

命令解释 ：

​        R2 --> R1    指的是 BFD 会话的名称

​       peer-ip          bfd会话对端设备的IP地址

​        source-ip     bfd本地设备的IP地址

​        discriminator local        bfd会话的本地标识符

​        discriminator remote    bfd传话远端标识符

​        commit                          提交 如果不写，是不生效的

### 3-8：动态 BFD

  在路由器1 和路由器 2之间也建立BFD(300MS)会话，是因为BFD的响应速度会远远大于VRRP响应速度（3S）当R1的返回数据bfd已检测到R2的GI0/0/1口当掉。默认路由不可取时，而vrrp备份网关还没有上位。变成主网关。

路由器 2 设置

```shell
[Route2]bfd R2-->R3 bind peer-ip 192.168.1.252
[Route2-bfd-session-r2-->r3]discriminator local 11
[Route2-bfd-session-r2-->r3]discriminator remote 2
[Route2-bfd-session-r2-->r3]commit 
[Route2-bfd-session-r2-->r3]q
[Route2]display bfd session all
--------------------------------------------------------------------------------
Local Remote     PeerIpAddr      State     Type        InterfaceName            
--------------------------------------------------------------------------------

1     2          192.168.13.2    Up        S_IP_PEER         -                  
11    2          192.168.1.252   Up        S_IP_PEER         -                  
--------------------------------------------------------------------------------
     Total UP/DOWN Session Number : 2/0
```

路由器 1 设置

```shell
[Route3]bfd 
[Route3-bfd]q	
[Route3]bfd R3-->R2 bind peer-ip 192.168.1.251	
[Route3-bfd-session-r3-->r2]discriminator local 2
[Route3-bfd-session-r3-->r2]discriminator remote 11
[Route3-bfd-session-r3-->r2]commit 
[Route3-bfd-session-r3-->r2]q
[Route3]display bfd session all
--------------------------------------------------------------------------------
Local Remote     PeerIpAddr      State     Type        InterfaceName            
--------------------------------------------------------------------------------

2     11         192.168.1.251   Up        S_IP_PEER         -                  
--------------------------------------------------------------------------------
     Total UP/DOWN Session Number : 1/0
```

在路由器 3 的GI0/0/2口，设置跟踪会话R3-R2来监控路由 2的GI0/0/1接口状态：

```java
[Route3-GigabitEthernet0/0/2]vrrp vrid 10 track bfd-session 2 increased 115
```

### 3-9: vrrp安全认证

```java
[Route2-GigabitEthernet0/0/1]vrrp vrid 1 authentication-mode md5 hcie
```

4：VRRP MSTP

![](../day06/images/vrrpandmstp.png)

课外作业
