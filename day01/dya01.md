#  Day01

## 1:  网络的形成和发展

​    网络的形成和发展总共分为四个阶段：

###    1-1 第一阶段 

​         20世纪60年代末到20世纪70年代初 ，为计算机发展的萌芽阶段 ，其主要特征是为了增加系统的计算能力和资源共享，是由美国国防部于1969年建成的。

###    1-2 第二阶段 

​         20世纪70年代中后期是局域网络（LAN）发展的 ， 其主要特征是局域网络作为一种新型的计算机体系结构开始进入生产部门，局域网技术是从远程分组

​        交换通信网络和 I/0 总线结构计算机派生出来的。

###     1-3 第三阶段

​          整个20世纪80年代是计算机局域网络的发展是期

​          其主要的特征是局域网完全从硬件上实现了ISO的开放系统互连通信模式协议的能力

###    1-4  第四阶段

​          20世纪90年代初至今是计算机网络飞速发展的阶段

​          其主要的特征是计算机网络化，协同计算能力发展以及全球互联网（internet）的盛行，

​          计算机的发展已经完全和网络融为一体，体现了“网络就是计算机”的口号

简单介绍一下BTC(比特币)

​         根据BTC官网的白皮书介绍，BTC是2009年1月由一个叫中本聪哥们自己写的一段代码，然后自已用自己的电脑的CPU挖出50个比特币。当时比特币的汇率是 1美元 = *****.03 btc  比特币的首次交易是在，佛罗里达州一个程序员有******个比特币买了两个披萨，这可能是全球最贵的披萨。截止目前，比特币的汇率为1 BTC = ******.* 美元 股市有风险，投资需谨慎！BTC的爆发是在2018年 勒索**（永恒之蓝）带着BTC走进大众的视野，** ******* 互联网

## 2：网络设备和网络传输介质

###    2-1：网络设备 

####        2-1-1  防火墙

​                 防火墙（Firewalld）是局域网接入互联网的一个边缘设备，他主要是对网络中的数据流量一个管控机制，所有的管控机制是由一个个控制列表组成，俗称”策略“。主要是允许和阻止数据流量通过和禁止。安全-通过 危险-禁止

![](../day01/images/firewalldend.jpg)

#### 2-1-2   路由器

​           路由器（Route）是连接两个或多个网络的硬件设备，在网络间起网关的作用，是一个网络到达别一个网络的关口，也是数据转发的地方。是读取每一个数据包中的地址然后决定如何快速智能的传送的网络设备。它能理解不同的协议，局域网中的以太网协议，互联网中的TCP/IP协议。还可以按照路由算法，按最佳路线将数据包传送到指定位置。（路由表）

![](../day01/images/route.jpg)

#### 2-1-3   交换机

​           交换机（Switch）是一种信号（电/光）转发的网络设备，他可以在网络中任意连接两个或多个网络节点，提供独享的数据通道，最常见的有以太网交换机，电话交换机，光纤交换机。

![](../day01/images/Switch.jpg)

2-1-4  网卡

​           网卡（Ethernet ）是用来允许计算机在计算机网络上过行通讯的计算机硬件。由于其拥有MAC地址，因此属于OSI网络模型的第1层和第2层之间。它使得用户可以通过电缆或无线相互连接通讯。

![](../day01/images/Ethinter.jpg)

### 2-2 网络传输介质

####       2-2-1  电缆   

​                主要应用于骨干、主干网络网络（12芯 24芯）

####       2-2-2  光纤   

​                 主要应用设置之间的连接（跳线、分单模和多模，单模为黄色 多模为橙色）

####      2-2-3  网线

​                 主要应用设备之间的连接（双绞线 跳线 分百兆和千兆 设备灯颜色区分 绿色为百兆 橙色为千兆）

​                线序的规范：

​                 T568A   白绿 绿 白橙 蓝 白蓝 橙 白棕 棕  （直通线）

​                 T568C  白橙 橙 白绿 蓝 蓝白 绿 白棕  棕 （交叉线）

####     2-2-4 等离子芯片

​                目前还在研发，未来可期！

![](../day01/images/tiaoxian.jpg)



## 3： 华为模拟器eNSP网络仿真平台

####          3-1：eNSP介绍

​              华为公司研发的一款图形化网络仿真平台，便于 ICT（信息通信技术）从业者快速熟悉华为产品，可以实现企业网络规划/设计/运维等相关操作，帮助使用者了解并 掌握相关操作和配置

![](../day01/images/ensp.png)

####      3-2：eNSP安装

​              从华为官网下载最新版eNSP安装包文件 ，比如  eNSP V100R002C00B510 Setup.exe ,鼠标右键以管理员身份运行安装程序，根据提示完成eNSP

![](../day01/images/ensp.setup.jpg)

​              提示：防火墙（软件）要允许eNSP网络通信

​              win+R 调用 firewall.cpl，可快速打开防火墙配置

​    ![](../day01/images/enspfirewall.jpg)

#### 3-3:  熟悉eNSP界面基本操作

​       图标功能 注册设备 CLS字休 颜色 透明度 网格等等的基本设置

## 4： 华为 VRP系统的基本操作

###               4-1：dir .   

```java
<Huawei>dir                           //查看当目录下的文件及文件夹
Directory of flash:/

  Idx  Attr     Size(Byte)  Date        Time       FileName 
    0  drw-              -  Aug 06 2015 21:26:42   src
    1  drw-              -  Oct 25 2021 17:27:03   compatible

32,004 KB total (31,972 KB free)

```

###          4-2：mkdir 文件夹名 . 

```java
<Huawei>mkdir xaliupeng             //创建一个名为xaliupeng文件夹
Info: Create directory flash:/xaliupeng......Done.
<Huawei>dir
Directory of flash:/

  Idx  Attr     Size(Byte)  Date        Time       FileName 
    0  drw-              -  Aug 06 2015 21:26:42   src
    1  drw-              -  Oct 25 2021 17:27:03   compatible
    2  drw-              -  Oct 25 2021 17:50:47   xaliupeng

32,004 KB total (31,968 KB free)

```

###           4-3：cd  文件夹名 .   pwd查看目录

```java
<Huawei>cd xaliupeng/                        //进入xaliupeng目录
<Huawei>pwd
flash:/xaliupeng                             //显示当前的目录
```

###            4-5：mkdir  文件名

```java
<Huawei>mkdir shutong.txt                   //新建一个shutong.txt文本文件
Info: Create directory flash:/xaliupeng/shutong.txt......Done.   
```

###           4-6：rm  文件名

```java
<Huawei>dir                                //查看当前目录下的文件flash:/xaliupeng
Directory of flash:/xaliupeng/

  Idx  Attr     Size(Byte)  Date        Time       FileName 
    0  drw-              -  Oct 25 2021 17:58:17   shutong.txt

32,004 KB total (31,964 KB free)
<Huawei>rm shutong.txt/ 
Remove directory flash:/xaliupeng/shutong.txt?[Y/N]:y
%Removing directory flash:/xaliupeng/shutong.txt...Done!
<Huawei>dir
Info: File can't be found in the directory.
32,004 KB total (31,968 KB free)

```

###     4-7：清屏

```JAVA
<Huawei>undo terminal monitor                //在eNSP模拟器中的CLI右击选择清屏
```

###     4-8：cd ..

```java
<Huawei>cd ..                                //返回上一级目录
```

### 4-9：save

```java
<Huawei>save tedu.zip                        //文件保存为tedu.zip
Are you sure to save the configuration to flash:/tedu.zip?[Y/N]:y
Now saving the current configuration to the slot 0.
Save the configuration successfully.
<Huawei>dir                                  //查看
Directory of flash:/

  Idx  Attr     Size(Byte)  Date        Time       FileName 
    0  drw-              -  Aug 06 2015 21:26:42   src
    1  drw-              -  Oct 26 2021 09:17:52   compatible
    2  -rw-            445  Oct 26 2021 09:43:02   tedu.zip

32,004 KB total (31,952 KB free)
```

### 4-10：copy

```java
<Huawei>copy tedu.zip ./xaliupeng/          //拷贝tedu.zip到本地目录xaliupeng目录中
Copy flash:/tedu.zip to flash:/xaliupeng/tedu.zip?[Y/N]:y

100%  complete
Info: Copied file flash:/tedu.zip to flash:/xaliupeng/tedu.zip...Done.
```

### 4-11：delete

```java
<Huawei>delete tedu.zip                   //删除tedu.zip文件
Delete flash:/xaliupeng/tedu.zip?[Y/N]:y
Info: Deleting file flash:/xaliupeng/tedu.zip...succeeded.

<Huawei>delete xaliupeng/                 //删除文件夹xaliupeng/
Delete flash:/xaliupeng/yulincollege.zip?[Y/N]:y
Info: Deleting file flash:/xaliupeng/yulincollege.zip...succeeded.  
        
```

##              5：华为路由器、交换机的四种视图模式

###           5-1：用户视图      

```java
<Huawei>system-view                           //<Huawei>带双尖括号为用户视图，操作的权限受限
```

###           5-2：系统视图

```java
[Huawei]interface GigabitEthernet 0/0/1       //<Huawei>带中括号为系统视图，操作的权限最高级
```

###          5-3：接口视图

```java
[Huawei-GigabitEthernet0/0/1]shutdown         //GigabitEthernet0/0/1 进入接口模式
```

###          5-4：协议视图

```java
[Huawei-ospfv3-1]                             //ospf动态路由协议
```

## 6：恢复出厂设置

​     适用于系统配置紊乱/错误、修复不便等情况

```java
<Tedu>reset saved-configuration               //恢复出厂设置
Warning: The action will delete the saved configuration in the device.
The configuration will be erased to reconfigure. Continue? [Y/N]:y      //此操作会删除系统中所有的配置询问您是否继续
Warning: Now clearing the configuration in the device.
Oct 26 2021 11:04:25-08:00 Tedu %%01CFM/4/RST_CFG(l)[54]:The user chose Y when d
eciding whether to reset the saved configuration.
Info: Succeeded in clearing the configuration in the device.
```

```java
<Tedu>reboot                                 //重启
Info: The system is now comparing the configuration, please wait.
Warning: All the configuration will be saved to the configuration file for the n
ext startup:, Continue?[Y/N]:n              //所有的配置下次重启会重新加载
```

```java
Info: If want to reboot with saving diagnostic information, input 'N' and then e
xecute 'reboot save diagnostic-information'.
System will reboot! Continue?[Y/N]:y        //系统重启，是否继续
```

